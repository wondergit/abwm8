Application.$controller("pFinNavPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */
    };
    $scope.ancStatementClick = function($event, $isolateScope) {
        $scope.Widgets.nav2_1.show = "false"
        $scope.$parent.Widgets.contFin.content = "finpStmnt"
    };


    $scope.ancCashbookClick = function($event, $isolateScope) {
        $scope.Widgets.nav2_1.show = "true"
        $scope.Widgets.nav1_1.show = "false"
    };


    $scope.ancULStateClick = function($event, $isolateScope) {
        $scope.$parent.Widgets.contFin.content = "finpCUpload"

    };




    $scope.ancCBIdRemClick = function($event, $isolateScope) {
        $scope.$parent.Widgets.contFin.content = "finpCBIdRem"

    };


    $scope.ancViewCBClick = function($event, $isolateScope) {
        $scope.$parent.Widgets.contFin.content = "finpCBVCb"

    };


    $scope.ancDedChgClick = function($event, $isolateScope) {
        $scope.Widgets.nav2_1.show = "false"
        $scope.$parent.Widgets.contFin.content = "finpDedChg"
    };

    $scope.ancPayClick = function($event, $isolateScope) {
        $scope.Widgets.nav2_1.show = "false"
        $scope.$parent.Widgets.contFin.content = "finpPay"

    };

}]);