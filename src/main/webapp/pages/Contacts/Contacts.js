Application.$controller("ContactsPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */
    };


    $scope.ancCntMethodsClick = function($event, $isolateScope) {
        $scope.Widgets.contCnt.content = "pCntMethods";
    };
    $scope.ancCntTaxClick = function($event, $isolateScope) {
        $scope.Widgets.contCnt.content = "pCntTax";
    };
    $scope.ancCntBankClick = function($event, $isolateScope) {
        $scope.Widgets.contCnt.content = "pCntBank";
    };

    $scope.ancCntPaySplitClick = function($event, $isolateScope) {
        $scope.Widgets.contCnt.content = "pCntPaySplits";
    };
    $scope.ancCommClick = function($event, $isolateScope) {
        $scope.Widgets.contCnt.content = "pCntComm";
    };
    $scope.ancRelationClick = function($event, $isolateScope) {
        $scope.Widgets.contCnt.content = "pCntRelations";
    };

    $scope.ancCntWorksClick = function($event, $isolateScope) {
        $scope.Widgets.contCnt.content = "pCntWorks";
    };
    $scope.ancCntSubmissionsClick = function($event, $isolateScope) {
        $scope.Widgets.contCnt.content = "pCntSubmission";
    };
    $scope.ancCntContractsClick = function($event, $isolateScope) {
        $scope.Widgets.contCnt.content = "pCntContracts";
    };
    $scope.ancCntPaymentsClick = function($event, $isolateScope) {
        $scope.Widgets.contCnt.content = "pCntPayments";
    };
    $scope.ancCntRelationClick = function($event, $isolateScope) {
        $scope.Widgets.contCnt.content = "pCntRelations";
    };




    $scope.btnSrchPseudClick = function($event, $isolateScope) {
        $scope.Variables.statVarSearchType.datavalue = 1;
    };


    $scope.bEditClick = function($event, $isolateScope) {
        $scope.Variables.EditMode = {
            "dataValue": 1
        };
        $scope.Widgets.tVarTest.dataValue = $scope.Variables.EditMode.dataValue;
    };


    $scope.btnSrchEmplClick = function($event, $isolateScope) {
        $scope.Variables.CompanySearch = {
            "dataValue": 1
        };
        $scope.Variables.lvSearchContacts.update();
    };


    $scope.gContactsSelect = function($event, $data) {
        if ($scope.Widgets.gContacts.selecteditem.iscompany === false) {
            $scope.Widgets.contCntDetails.content = "pCntPerson";
        } else if ($scope.Widgets.gContacts.selecteditem.iscompany === true) {
            $scope.Widgets.contCntDetails.content = "pCntCompany";
        } else {
            $scope.Widgets.contCntDetails.content = "pNoRecords";
        }
    };


    $scope.gContactsDeselect = function($event, $data) {
        $scope.Widgets.contCntDetails.content = "pNoRecords";
    };


    $scope.gContactsClick = function($event, $data) {
        if ($scope.Widgets.gContacts.selecteditem.iscompany === false) {
            $scope.Widgets.contCntDetails.content = "pCntPerson";
        } else if ($scope.Widgets.gContacts.selecteditem.iscompany === true) {
            $scope.Widgets.contCntDetails.content = "pCntCompany";
        } else {
            $scope.Widgets.contCntDetails.content = "pNoRecords";
        }
    };

}]);


Application.$controller("gContactsController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);



Application.$controller("dAddCntController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("dAddWkController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("dAddCtrtController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("dialog4Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("pageDiaContactSearchController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);



Application.$controller("grid3Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("grEmployeesController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("grEmpContactController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("grid6Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);