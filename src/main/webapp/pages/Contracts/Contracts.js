Application.$controller("ContractsPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */
    };



    $scope.ancDatesClick = function($event, $isolateScope) {
        $scope.Widgets.contCtrt.content = "pCtrtDates"
    };


    $scope.ancRespClick = function($event, $isolateScope) {
        $scope.Widgets.contCtrt.content = "pCtrtResp"
    };


    $scope.ancPayMileClick = function($event, $isolateScope) {
        $scope.Widgets.contCtrt.content = "pCtrtPayMile"
    };


    $scope.ancFormatsClick = function($event, $isolateScope) {
        $scope.Widgets.contCtrt.content = "pCtrtFormats"
    };


    $scope.ancRightsClick = function($event, $isolateScope) {
        $scope.Widgets.contCtrt.content = "pCtrtRights"
    };


    $scope.ancSubRightsClick = function($event, $isolateScope) {
        $scope.Widgets.contCtrt.content = "pCtrtSubRights"
    };


    $scope.anc3PPClick = function($event, $isolateScope) {
        $scope.Widgets.contCtrt.content = "pCtrt3PP"
    };


    $scope.ancRelContClick = function($event, $isolateScope) {
        $scope.Widgets.contCtrt.content = "pCtrtRelCont"
    };


    $scope.ancPaymentsClick = function($event, $isolateScope) {
        $scope.Widgets.contCtrt.content = ""
    };

}]);


Application.$controller("grid1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);