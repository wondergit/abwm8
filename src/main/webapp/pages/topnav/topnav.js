Application.$controller("topnavPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */
    };
    $scope.bHomeClick = function($event, $isolateScope) {
        $scope.$parent.Widgets.maincontainer.content = "Home";
        $scope.bHome.backgroundcolor = "#CCCCCC";
        $scope.bHome.color = "#ffffff"
        $scope.bContacts.backgroundcolor = "";
        $scope.bContactsClick.color = "";
    };


    $scope.bContactsClick = function($event, $isolateScope) {
        $scope.$parent.Widgets.maincontainer.content = "Contacts"
    };


    $scope.bWorksClick = function($event, $isolateScope) {
        $scope.$parent.Widgets.maincontainer.content = "Works"
    };


    $scope.bContractsClick = function($event, $isolateScope) {
        $scope.$parent.Widgets.maincontainer.content = "Contracts"
    };


    $scope.bFinanceClick = function($event, $isolateScope) {
        $scope.$parent.Widgets.maincontainer.content = "Finance"
    };


}]);