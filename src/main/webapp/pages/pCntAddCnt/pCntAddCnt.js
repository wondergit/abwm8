Application.$controller("pCntAddCntPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */
    };

    $scope.radioset1Click = function($event, $isolateScope) {

    }
    $scope.bAddCntStartClick = function($event, $isolateScope) {
        if ($scope.Widgets.radioset1.selectedItem == "Person") {
            $scope.Variables.navToPers.navigate()
        } else {
            $scope.Variables.navToCo.navigate()
        }

    };
}]);