/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Column;
import javax.persistence.Embeddable;


import java.util.Arrays;

import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.UniqueConstraint;




/**
 * LuTermTypeTransId generated by hbm2java
 */
@Embeddable
public class LuTermTypeTransId  implements java.io.Serializable
 {


private Integer luLanguageid;
private Integer luTermTypesid;

    public LuTermTypeTransId() {
    }




    

    @Column(name="lu_languageid", nullable=false)
    public Integer getLuLanguageid() {
        return this.luLanguageid;
    }
    
    public void setLuLanguageid(Integer luLanguageid) {
        this.luLanguageid = luLanguageid;
    }

    

    @Column(name="lu_term_typesid", nullable=false)
    public Integer getLuTermTypesid() {
        return this.luTermTypesid;
    }
    
    public void setLuTermTypesid(Integer luTermTypesid) {
        this.luTermTypesid = luTermTypesid;
    }



public boolean equals(Object o) {
         if (this == o)
         return true;
		 if ( (o == null ) )
		 return false;
		 if ( !(o instanceof LuTermTypeTransId) )
		 return false;

		 LuTermTypeTransId that = ( LuTermTypeTransId ) o;

         return ( (this.getLuLanguageid()==that.getLuLanguageid()) || ( this.getLuLanguageid()!=null && that.getLuLanguageid()!=null && this.getLuLanguageid().equals(that.getLuLanguageid()) ) )
 && ( (this.getLuTermTypesid()==that.getLuTermTypesid()) || ( this.getLuTermTypesid()!=null && that.getLuTermTypesid()!=null && this.getLuTermTypesid().equals(that.getLuTermTypesid()) ) );

   }

   public int hashCode() {
         int result = 17;

         result = 37 * result + ( getLuLanguageid() == null ? 0 : this.getLuLanguageid().hashCode() );
         result = 37 * result + ( getLuTermTypesid() == null ? 0 : this.getLuTermTypesid().hashCode() );

         return result;
     }


}

