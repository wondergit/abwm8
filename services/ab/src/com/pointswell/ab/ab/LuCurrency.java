/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import javax.persistence.PrimaryKeyJoinColumn;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import java.util.Arrays;

import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.UniqueConstraint;




/**
 * LuCurrency generated by hbm2java
 */
@Entity
@Table(name="lu_currency"
)
public class LuCurrency  implements java.io.Serializable
 {


private Integer luCurrencyid;
private String luCurrencyCode;
private String luCurrencyName;
private Integer luCurrencySort;
private Date effectivefrom;
private Date effectiveto;
private Set<CtrtPayMilestone> ctrtPayMilestones = new HashSet<CtrtPayMilestone>(0);
private Set<PmtNominalLedger> pmtNominalLedgers = new HashSet<PmtNominalLedger>(0);
private Set<CntBankdetails> cntBankdetailses = new HashSet<CntBankdetails>(0);
private Set<PmtContactAcc> pmtContactAccs = new HashSet<PmtContactAcc>(0);
private Set<PmtAgencyVat> pmtAgencyVats = new HashSet<PmtAgencyVat>(0);
private Set<PmtRoyalties> pmtRoyaltieses = new HashSet<PmtRoyalties>(0);
private Set<CtrtDeliverable> ctrtDeliverables = new HashSet<CtrtDeliverable>(0);
private Set<PmtDeductions> pmtDeductionses = new HashSet<PmtDeductions>(0);
private Set<WkHasContributor> wkHasContributors = new HashSet<WkHasContributor>(0);
private Set<PmtContractControl> pmtContractControls = new HashSet<PmtContractControl>(0);
private Set<Contract> contracts = new HashSet<Contract>(0);
private Set<PmtBankAccount> pmtBankAccounts = new HashSet<PmtBankAccount>(0);
private Set<PmtFxGainloss> pmtFxGainlossesForPmtFxGlSourcecurr = new HashSet<PmtFxGainloss>(0);
private Set<PmtFxGainloss> pmtFxGainlossesForPmtFxGlTargetcurr = new HashSet<PmtFxGainloss>(0);
private Set<Tenant> tenants = new HashSet<Tenant>(0);
private Set<PmtJournal> pmtJournals = new HashSet<PmtJournal>(0);

    public LuCurrency() {
    }



     @Id @GeneratedValue(strategy=IDENTITY)

    

    @Column(name="lu_currencyid", nullable=false)
    public Integer getLuCurrencyid() {
        return this.luCurrencyid;
    }
    
    public void setLuCurrencyid(Integer luCurrencyid) {
        this.luCurrencyid = luCurrencyid;
    }

    

    @Column(name="lu_currency_code", nullable=false, length=3)
    public String getLuCurrencyCode() {
        return this.luCurrencyCode;
    }
    
    public void setLuCurrencyCode(String luCurrencyCode) {
        this.luCurrencyCode = luCurrencyCode;
    }

    

    @Column(name="lu_currency_name", nullable=false, length=45)
    public String getLuCurrencyName() {
        return this.luCurrencyName;
    }
    
    public void setLuCurrencyName(String luCurrencyName) {
        this.luCurrencyName = luCurrencyName;
    }

    

    @Column(name="lu_currency_sort", nullable=false)
    public Integer getLuCurrencySort() {
        return this.luCurrencySort;
    }
    
    public void setLuCurrencySort(Integer luCurrencySort) {
        this.luCurrencySort = luCurrencySort;
    }

    @Temporal(TemporalType.DATE)

    @Column(name="effectivefrom", nullable=false, length=10)
    public Date getEffectivefrom() {
        return this.effectivefrom;
    }
    
    public void setEffectivefrom(Date effectivefrom) {
        this.effectivefrom = effectivefrom;
    }

    @Temporal(TemporalType.DATE)

    @Column(name="effectiveto", length=10)
    public Date getEffectiveto() {
        return this.effectiveto;
    }
    
    public void setEffectiveto(Date effectiveto) {
        this.effectiveto = effectiveto;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luCurrency")
    public Set<CtrtPayMilestone> getCtrtPayMilestones() {
        return this.ctrtPayMilestones;
    }
    
    public void setCtrtPayMilestones(Set<CtrtPayMilestone> ctrtPayMilestones) {
        this.ctrtPayMilestones = ctrtPayMilestones;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luCurrency")
    public Set<PmtNominalLedger> getPmtNominalLedgers() {
        return this.pmtNominalLedgers;
    }
    
    public void setPmtNominalLedgers(Set<PmtNominalLedger> pmtNominalLedgers) {
        this.pmtNominalLedgers = pmtNominalLedgers;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luCurrency")
    public Set<CntBankdetails> getCntBankdetailses() {
        return this.cntBankdetailses;
    }
    
    public void setCntBankdetailses(Set<CntBankdetails> cntBankdetailses) {
        this.cntBankdetailses = cntBankdetailses;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luCurrency")
    public Set<PmtContactAcc> getPmtContactAccs() {
        return this.pmtContactAccs;
    }
    
    public void setPmtContactAccs(Set<PmtContactAcc> pmtContactAccs) {
        this.pmtContactAccs = pmtContactAccs;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luCurrency")
    public Set<PmtAgencyVat> getPmtAgencyVats() {
        return this.pmtAgencyVats;
    }
    
    public void setPmtAgencyVats(Set<PmtAgencyVat> pmtAgencyVats) {
        this.pmtAgencyVats = pmtAgencyVats;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luCurrency")
    public Set<PmtRoyalties> getPmtRoyaltieses() {
        return this.pmtRoyaltieses;
    }
    
    public void setPmtRoyaltieses(Set<PmtRoyalties> pmtRoyaltieses) {
        this.pmtRoyaltieses = pmtRoyaltieses;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luCurrency")
    public Set<CtrtDeliverable> getCtrtDeliverables() {
        return this.ctrtDeliverables;
    }
    
    public void setCtrtDeliverables(Set<CtrtDeliverable> ctrtDeliverables) {
        this.ctrtDeliverables = ctrtDeliverables;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luCurrency")
    public Set<PmtDeductions> getPmtDeductionses() {
        return this.pmtDeductionses;
    }
    
    public void setPmtDeductionses(Set<PmtDeductions> pmtDeductionses) {
        this.pmtDeductionses = pmtDeductionses;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luCurrency")
    public Set<WkHasContributor> getWkHasContributors() {
        return this.wkHasContributors;
    }
    
    public void setWkHasContributors(Set<WkHasContributor> wkHasContributors) {
        this.wkHasContributors = wkHasContributors;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luCurrency")
    public Set<PmtContractControl> getPmtContractControls() {
        return this.pmtContractControls;
    }
    
    public void setPmtContractControls(Set<PmtContractControl> pmtContractControls) {
        this.pmtContractControls = pmtContractControls;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luCurrency")
    public Set<Contract> getContracts() {
        return this.contracts;
    }
    
    public void setContracts(Set<Contract> contracts) {
        this.contracts = contracts;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luCurrency")
    public Set<PmtBankAccount> getPmtBankAccounts() {
        return this.pmtBankAccounts;
    }
    
    public void setPmtBankAccounts(Set<PmtBankAccount> pmtBankAccounts) {
        this.pmtBankAccounts = pmtBankAccounts;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luCurrencyByPmtFxGlSourcecurr")
    public Set<PmtFxGainloss> getPmtFxGainlossesForPmtFxGlSourcecurr() {
        return this.pmtFxGainlossesForPmtFxGlSourcecurr;
    }
    
    public void setPmtFxGainlossesForPmtFxGlSourcecurr(Set<PmtFxGainloss> pmtFxGainlossesForPmtFxGlSourcecurr) {
        this.pmtFxGainlossesForPmtFxGlSourcecurr = pmtFxGainlossesForPmtFxGlSourcecurr;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luCurrencyByPmtFxGlTargetcurr")
    public Set<PmtFxGainloss> getPmtFxGainlossesForPmtFxGlTargetcurr() {
        return this.pmtFxGainlossesForPmtFxGlTargetcurr;
    }
    
    public void setPmtFxGainlossesForPmtFxGlTargetcurr(Set<PmtFxGainloss> pmtFxGainlossesForPmtFxGlTargetcurr) {
        this.pmtFxGainlossesForPmtFxGlTargetcurr = pmtFxGainlossesForPmtFxGlTargetcurr;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luCurrency")
    public Set<Tenant> getTenants() {
        return this.tenants;
    }
    
    public void setTenants(Set<Tenant> tenants) {
        this.tenants = tenants;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luCurrency")
    public Set<PmtJournal> getPmtJournals() {
        return this.pmtJournals;
    }
    
    public void setPmtJournals(Set<PmtJournal> pmtJournals) {
        this.pmtJournals = pmtJournals;
    }



public boolean equals(Object o) {
         if (this == o)
         return true;
		 if ( (o == null ) )
		 return false;
		 if ( !(o instanceof LuCurrency) )
		 return false;

		 LuCurrency that = ( LuCurrency ) o;

		 return ( (this.getLuCurrencyid()==that.getLuCurrencyid()) || ( this.getLuCurrencyid()!=null && that.getLuCurrencyid()!=null && this.getLuCurrencyid().equals(that.getLuCurrencyid()) ) );

   }

   public int hashCode() {
         int result = 17;

         result = 37 * result + ( getLuCurrencyid() == null ? 0 : this.getLuCurrencyid().hashCode() );

         return result;
     }


}

