/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Column;
import javax.persistence.Embeddable;


import java.util.Arrays;

import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.UniqueConstraint;




/**
 * LuCtrtTypeTransId generated by hbm2java
 */
@Embeddable
public class LuCtrtTypeTransId  implements java.io.Serializable
 {


private Integer luCtrtTypeid;
private Integer luLanguageid;

    public LuCtrtTypeTransId() {
    }




    

    @Column(name="lu_ctrt_typeid", nullable=false)
    public Integer getLuCtrtTypeid() {
        return this.luCtrtTypeid;
    }
    
    public void setLuCtrtTypeid(Integer luCtrtTypeid) {
        this.luCtrtTypeid = luCtrtTypeid;
    }

    

    @Column(name="lu_languageid", nullable=false)
    public Integer getLuLanguageid() {
        return this.luLanguageid;
    }
    
    public void setLuLanguageid(Integer luLanguageid) {
        this.luLanguageid = luLanguageid;
    }



public boolean equals(Object o) {
         if (this == o)
         return true;
		 if ( (o == null ) )
		 return false;
		 if ( !(o instanceof LuCtrtTypeTransId) )
		 return false;

		 LuCtrtTypeTransId that = ( LuCtrtTypeTransId ) o;

         return ( (this.getLuCtrtTypeid()==that.getLuCtrtTypeid()) || ( this.getLuCtrtTypeid()!=null && that.getLuCtrtTypeid()!=null && this.getLuCtrtTypeid().equals(that.getLuCtrtTypeid()) ) )
 && ( (this.getLuLanguageid()==that.getLuLanguageid()) || ( this.getLuLanguageid()!=null && that.getLuLanguageid()!=null && this.getLuLanguageid().equals(that.getLuLanguageid()) ) );

   }

   public int hashCode() {
         int result = 17;

         result = 37 * result + ( getLuCtrtTypeid() == null ? 0 : this.getLuCtrtTypeid().hashCode() );
         result = 37 * result + ( getLuLanguageid() == null ? 0 : this.getLuLanguageid().hashCode() );

         return result;
     }


}

