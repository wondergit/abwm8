/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import javax.persistence.PrimaryKeyJoinColumn;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;


import java.util.Arrays;

import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.UniqueConstraint;




/**
 * SysProcessLog generated by hbm2java
 */
@Entity
@Table(name="sys_process_log"
)
public class SysProcessLog  implements java.io.Serializable
 {


private Integer sysProcessLogid;
private String sysProcessLogName;
private Integer sysProcessLogRecordcount;


@Type(type="DateTime")
private LocalDateTime sysProcessLogStart;


@Type(type="DateTime")
private LocalDateTime sysProcessLogStop;
private Set<SysAuditLog> sysAuditLogs = new HashSet<SysAuditLog>(0);

    public SysProcessLog() {
    }



     @Id @GeneratedValue(strategy=IDENTITY)

    

    @Column(name="sys_process_logid", nullable=false)
    public Integer getSysProcessLogid() {
        return this.sysProcessLogid;
    }
    
    public void setSysProcessLogid(Integer sysProcessLogid) {
        this.sysProcessLogid = sysProcessLogid;
    }

    

    @Column(name="sys_process_log_name", nullable=false, length=45)
    public String getSysProcessLogName() {
        return this.sysProcessLogName;
    }
    
    public void setSysProcessLogName(String sysProcessLogName) {
        this.sysProcessLogName = sysProcessLogName;
    }

    

    @Column(name="sys_process_log_recordcount")
    public Integer getSysProcessLogRecordcount() {
        return this.sysProcessLogRecordcount;
    }
    
    public void setSysProcessLogRecordcount(Integer sysProcessLogRecordcount) {
        this.sysProcessLogRecordcount = sysProcessLogRecordcount;
    }

    

    @Column(name="sys_process_log_start", nullable=false)
    public LocalDateTime getSysProcessLogStart() {
        return this.sysProcessLogStart;
    }
    
    public void setSysProcessLogStart(LocalDateTime sysProcessLogStart) {
        this.sysProcessLogStart = sysProcessLogStart;
    }

    

    @Column(name="sys_process_log_stop")
    public LocalDateTime getSysProcessLogStop() {
        return this.sysProcessLogStop;
    }
    
    public void setSysProcessLogStop(LocalDateTime sysProcessLogStop) {
        this.sysProcessLogStop = sysProcessLogStop;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="sysProcessLog")
    public Set<SysAuditLog> getSysAuditLogs() {
        return this.sysAuditLogs;
    }
    
    public void setSysAuditLogs(Set<SysAuditLog> sysAuditLogs) {
        this.sysAuditLogs = sysAuditLogs;
    }



public boolean equals(Object o) {
         if (this == o)
         return true;
		 if ( (o == null ) )
		 return false;
		 if ( !(o instanceof SysProcessLog) )
		 return false;

		 SysProcessLog that = ( SysProcessLog ) o;

		 return ( (this.getSysProcessLogid()==that.getSysProcessLogid()) || ( this.getSysProcessLogid()!=null && that.getSysProcessLogid()!=null && this.getSysProcessLogid().equals(that.getSysProcessLogid()) ) );

   }

   public int hashCode() {
         int result = 17;

         result = 37 * result + ( getSysProcessLogid() == null ? 0 : this.getSysProcessLogid().hashCode() );

         return result;
     }


}

