/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import javax.persistence.PrimaryKeyJoinColumn;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import java.util.Arrays;

import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.UniqueConstraint;




/**
 * LuOrigination generated by hbm2java
 */
@Entity
@Table(name="lu_origination"
)
public class LuOrigination  implements java.io.Serializable
 {


private Integer luOriginationid;
private String luOriginationIstccode;
private String luOriginationName;
private Set<Work> works = new HashSet<Work>(0);
private Set<LuOriginationTrans> luOriginationTranses = new HashSet<LuOriginationTrans>(0);

    public LuOrigination() {
    }



     @Id @GeneratedValue(strategy=IDENTITY)

    

    @Column(name="lu_originationid", nullable=false)
    public Integer getLuOriginationid() {
        return this.luOriginationid;
    }
    
    public void setLuOriginationid(Integer luOriginationid) {
        this.luOriginationid = luOriginationid;
    }

    

    @Column(name="lu_origination_istccode", nullable=false, length=2)
    public String getLuOriginationIstccode() {
        return this.luOriginationIstccode;
    }
    
    public void setLuOriginationIstccode(String luOriginationIstccode) {
        this.luOriginationIstccode = luOriginationIstccode;
    }

    

    @Column(name="lu_origination_name", nullable=false, length=45)
    public String getLuOriginationName() {
        return this.luOriginationName;
    }
    
    public void setLuOriginationName(String luOriginationName) {
        this.luOriginationName = luOriginationName;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luOrigination")
    public Set<Work> getWorks() {
        return this.works;
    }
    
    public void setWorks(Set<Work> works) {
        this.works = works;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luOrigination")
    public Set<LuOriginationTrans> getLuOriginationTranses() {
        return this.luOriginationTranses;
    }
    
    public void setLuOriginationTranses(Set<LuOriginationTrans> luOriginationTranses) {
        this.luOriginationTranses = luOriginationTranses;
    }



public boolean equals(Object o) {
         if (this == o)
         return true;
		 if ( (o == null ) )
		 return false;
		 if ( !(o instanceof LuOrigination) )
		 return false;

		 LuOrigination that = ( LuOrigination ) o;

		 return ( (this.getLuOriginationid()==that.getLuOriginationid()) || ( this.getLuOriginationid()!=null && that.getLuOriginationid()!=null && this.getLuOriginationid().equals(that.getLuOriginationid()) ) );

   }

   public int hashCode() {
         int result = 17;

         result = 37 * result + ( getLuOriginationid() == null ? 0 : this.getLuOriginationid().hashCode() );

         return result;
     }


}

