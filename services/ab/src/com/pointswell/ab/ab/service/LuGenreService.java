/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;

/**
 * Service object for domain model class LuGenre.
 * @see com.pointswell.ab.ab.LuGenre
 */

public interface LuGenreService {
   /**
	 * Creates a new lugenre.
	 * 
	 * @param created
	 *            The information of the created lugenre.
	 * @return The created lugenre.
	 */
	public LuGenre create(LuGenre created);

	/**
	 * Deletes a lugenre.
	 * 
	 * @param lugenreId
	 *            The id of the deleted lugenre.
	 * @return The deleted lugenre.
	 * @throws EntityNotFoundException
	 *             if no lugenre is found with the given id.
	 */
	public LuGenre delete(Integer lugenreId) throws EntityNotFoundException;

	/**
	 * Finds all lugenres.
	 * 
	 * @return A list of lugenres.
	 */
	public Page<LuGenre> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<LuGenre> findAll(Pageable pageable);
	
	/**
	 * Finds lugenre by id.
	 * 
	 * @param id
	 *            The id of the wanted lugenre.
	 * @return The found lugenre. If no lugenre is found, this method returns
	 *         null.
	 */
	public LuGenre findById(Integer id) throws EntityNotFoundException;
	/**
	 * Updates the information of a lugenre.
	 * 
	 * @param updated
	 *            The information of the updated lugenre.
	 * @return The updated lugenre.
	 * @throws EntityNotFoundException
	 *             if no lugenre is found with given id.
	 */
	public LuGenre update(LuGenre updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the lugenres in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the lugenre.
	 */

	public long countAll();


    public Page<LuGenre> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}

