/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;

/**
 * Service object for domain model class CtrtFormatStock.
 * @see com.pointswell.ab.ab.CtrtFormatStock
 */

public interface CtrtFormatStockService {
   /**
	 * Creates a new ctrtformatstock.
	 * 
	 * @param created
	 *            The information of the created ctrtformatstock.
	 * @return The created ctrtformatstock.
	 */
	public CtrtFormatStock create(CtrtFormatStock created);

	/**
	 * Deletes a ctrtformatstock.
	 * 
	 * @param ctrtformatstockId
	 *            The id of the deleted ctrtformatstock.
	 * @return The deleted ctrtformatstock.
	 * @throws EntityNotFoundException
	 *             if no ctrtformatstock is found with the given id.
	 */
	public CtrtFormatStock delete(Integer ctrtformatstockId) throws EntityNotFoundException;

	/**
	 * Finds all ctrtformatstocks.
	 * 
	 * @return A list of ctrtformatstocks.
	 */
	public Page<CtrtFormatStock> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<CtrtFormatStock> findAll(Pageable pageable);
	
	/**
	 * Finds ctrtformatstock by id.
	 * 
	 * @param id
	 *            The id of the wanted ctrtformatstock.
	 * @return The found ctrtformatstock. If no ctrtformatstock is found, this method returns
	 *         null.
	 */
	public CtrtFormatStock findById(Integer id) throws EntityNotFoundException;
	/**
	 * Updates the information of a ctrtformatstock.
	 * 
	 * @param updated
	 *            The information of the updated ctrtformatstock.
	 * @return The updated ctrtformatstock.
	 * @throws EntityNotFoundException
	 *             if no ctrtformatstock is found with given id.
	 */
	public CtrtFormatStock update(CtrtFormatStock updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the ctrtformatstocks in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the ctrtformatstock.
	 */

	public long countAll();


    public Page<CtrtFormatStock> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}

