/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;

/**
 * Service object for domain model class WkPermHasCty.
 * @see com.pointswell.ab.ab.WkPermHasCty
 */

public interface WkPermHasCtyService {
   /**
	 * Creates a new wkpermhascty.
	 * 
	 * @param created
	 *            The information of the created wkpermhascty.
	 * @return The created wkpermhascty.
	 */
	public WkPermHasCty create(WkPermHasCty created);

	/**
	 * Deletes a wkpermhascty.
	 * 
	 * @param wkpermhasctyId
	 *            The id of the deleted wkpermhascty.
	 * @return The deleted wkpermhascty.
	 * @throws EntityNotFoundException
	 *             if no wkpermhascty is found with the given id.
	 */
	public WkPermHasCty delete(WkPermHasCtyId wkpermhasctyId) throws EntityNotFoundException;

	/**
	 * Finds all wkpermhasctys.
	 * 
	 * @return A list of wkpermhasctys.
	 */
	public Page<WkPermHasCty> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<WkPermHasCty> findAll(Pageable pageable);
	
	/**
	 * Finds wkpermhascty by id.
	 * 
	 * @param id
	 *            The id of the wanted wkpermhascty.
	 * @return The found wkpermhascty. If no wkpermhascty is found, this method returns
	 *         null.
	 */
	public WkPermHasCty findById(WkPermHasCtyId id) throws EntityNotFoundException;
	/**
	 * Updates the information of a wkpermhascty.
	 * 
	 * @param updated
	 *            The information of the updated wkpermhascty.
	 * @return The updated wkpermhascty.
	 * @throws EntityNotFoundException
	 *             if no wkpermhascty is found with given id.
	 */
	public WkPermHasCty update(WkPermHasCty updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the wkpermhasctys in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the wkpermhascty.
	 */

	public long countAll();


    public Page<WkPermHasCty> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}

