/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;

/**
 * Service object for domain model class LuMethodTrans.
 * @see com.pointswell.ab.ab.LuMethodTrans
 */

public interface LuMethodTransService {
   /**
	 * Creates a new lumethodtrans.
	 * 
	 * @param created
	 *            The information of the created lumethodtrans.
	 * @return The created lumethodtrans.
	 */
	public LuMethodTrans create(LuMethodTrans created);

	/**
	 * Deletes a lumethodtrans.
	 * 
	 * @param lumethodtransId
	 *            The id of the deleted lumethodtrans.
	 * @return The deleted lumethodtrans.
	 * @throws EntityNotFoundException
	 *             if no lumethodtrans is found with the given id.
	 */
	public LuMethodTrans delete(LuMethodTransId lumethodtransId) throws EntityNotFoundException;

	/**
	 * Finds all lumethodtranss.
	 * 
	 * @return A list of lumethodtranss.
	 */
	public Page<LuMethodTrans> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<LuMethodTrans> findAll(Pageable pageable);
	
	/**
	 * Finds lumethodtrans by id.
	 * 
	 * @param id
	 *            The id of the wanted lumethodtrans.
	 * @return The found lumethodtrans. If no lumethodtrans is found, this method returns
	 *         null.
	 */
	public LuMethodTrans findById(LuMethodTransId id) throws EntityNotFoundException;
	/**
	 * Updates the information of a lumethodtrans.
	 * 
	 * @param updated
	 *            The information of the updated lumethodtrans.
	 * @return The updated lumethodtrans.
	 * @throws EntityNotFoundException
	 *             if no lumethodtrans is found with given id.
	 */
	public LuMethodTrans update(LuMethodTrans updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the lumethodtranss in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the lumethodtrans.
	 */

	public long countAll();


    public Page<LuMethodTrans> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}

