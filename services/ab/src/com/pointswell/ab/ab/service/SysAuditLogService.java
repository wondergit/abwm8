/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;

/**
 * Service object for domain model class SysAuditLog.
 * @see com.pointswell.ab.ab.SysAuditLog
 */

public interface SysAuditLogService {
   /**
	 * Creates a new sysauditlog.
	 * 
	 * @param created
	 *            The information of the created sysauditlog.
	 * @return The created sysauditlog.
	 */
	public SysAuditLog create(SysAuditLog created);

	/**
	 * Deletes a sysauditlog.
	 * 
	 * @param sysauditlogId
	 *            The id of the deleted sysauditlog.
	 * @return The deleted sysauditlog.
	 * @throws EntityNotFoundException
	 *             if no sysauditlog is found with the given id.
	 */
	public SysAuditLog delete(Integer sysauditlogId) throws EntityNotFoundException;

	/**
	 * Finds all sysauditlogs.
	 * 
	 * @return A list of sysauditlogs.
	 */
	public Page<SysAuditLog> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<SysAuditLog> findAll(Pageable pageable);
	
	/**
	 * Finds sysauditlog by id.
	 * 
	 * @param id
	 *            The id of the wanted sysauditlog.
	 * @return The found sysauditlog. If no sysauditlog is found, this method returns
	 *         null.
	 */
	public SysAuditLog findById(Integer id) throws EntityNotFoundException;
	/**
	 * Updates the information of a sysauditlog.
	 * 
	 * @param updated
	 *            The information of the updated sysauditlog.
	 * @return The updated sysauditlog.
	 * @throws EntityNotFoundException
	 *             if no sysauditlog is found with given id.
	 */
	public SysAuditLog update(SysAuditLog updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the sysauditlogs in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the sysauditlog.
	 */

	public long countAll();


    public Page<SysAuditLog> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}

