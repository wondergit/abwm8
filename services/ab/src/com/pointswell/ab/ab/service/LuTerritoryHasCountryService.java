/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;

/**
 * Service object for domain model class LuTerritoryHasCountry.
 * @see com.pointswell.ab.ab.LuTerritoryHasCountry
 */

public interface LuTerritoryHasCountryService {
   /**
	 * Creates a new luterritoryhascountry.
	 * 
	 * @param created
	 *            The information of the created luterritoryhascountry.
	 * @return The created luterritoryhascountry.
	 */
	public LuTerritoryHasCountry create(LuTerritoryHasCountry created);

	/**
	 * Deletes a luterritoryhascountry.
	 * 
	 * @param luterritoryhascountryId
	 *            The id of the deleted luterritoryhascountry.
	 * @return The deleted luterritoryhascountry.
	 * @throws EntityNotFoundException
	 *             if no luterritoryhascountry is found with the given id.
	 */
	public LuTerritoryHasCountry delete(LuTerritoryHasCountryId luterritoryhascountryId) throws EntityNotFoundException;

	/**
	 * Finds all luterritoryhascountrys.
	 * 
	 * @return A list of luterritoryhascountrys.
	 */
	public Page<LuTerritoryHasCountry> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<LuTerritoryHasCountry> findAll(Pageable pageable);
	
	/**
	 * Finds luterritoryhascountry by id.
	 * 
	 * @param id
	 *            The id of the wanted luterritoryhascountry.
	 * @return The found luterritoryhascountry. If no luterritoryhascountry is found, this method returns
	 *         null.
	 */
	public LuTerritoryHasCountry findById(LuTerritoryHasCountryId id) throws EntityNotFoundException;
	/**
	 * Updates the information of a luterritoryhascountry.
	 * 
	 * @param updated
	 *            The information of the updated luterritoryhascountry.
	 * @return The updated luterritoryhascountry.
	 * @throws EntityNotFoundException
	 *             if no luterritoryhascountry is found with given id.
	 */
	public LuTerritoryHasCountry update(LuTerritoryHasCountry updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the luterritoryhascountrys in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the luterritoryhascountry.
	 */

	public long countAll();


    public Page<LuTerritoryHasCountry> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}

