/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;


/**
 * ServiceImpl object for domain model class LuPaymentMethodsTrans.
 * @see com.pointswell.ab.ab.LuPaymentMethodsTrans
 */
@Service("ab.LuPaymentMethodsTransService")
public class LuPaymentMethodsTransServiceImpl implements LuPaymentMethodsTransService {


    private static final Logger LOGGER = LoggerFactory.getLogger(LuPaymentMethodsTransServiceImpl.class);

    @Autowired
    @Qualifier("ab.LuPaymentMethodsTransDao")
    private WMGenericDao<LuPaymentMethodsTrans, LuPaymentMethodsTransId> wmGenericDao;
    public void setWMGenericDao(WMGenericDao<LuPaymentMethodsTrans, LuPaymentMethodsTransId> wmGenericDao){
        this.wmGenericDao = wmGenericDao;
    }
     @Transactional(readOnly = true, value = "abTransactionManager")
     public Page<LuPaymentMethodsTrans> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable){
          LOGGER.debug("Fetching all associated");
          return this.wmGenericDao.getAssociatedObjects(value, entityName, key, pageable);
     }

    @Transactional(value = "abTransactionManager")
    @Override
    public LuPaymentMethodsTrans create(LuPaymentMethodsTrans lupaymentmethodstrans) {
        LOGGER.debug("Creating a new lupaymentmethodstrans with information: {}" , lupaymentmethodstrans);
        return this.wmGenericDao.create(lupaymentmethodstrans);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "abTransactionManager")
    @Override
    public LuPaymentMethodsTrans delete(LuPaymentMethodsTransId lupaymentmethodstransId) throws EntityNotFoundException {
        LOGGER.debug("Deleting lupaymentmethodstrans with id: {}" , lupaymentmethodstransId);
        LuPaymentMethodsTrans deleted = this.wmGenericDao.findById(lupaymentmethodstransId);
        if (deleted == null) {
            LOGGER.debug("No lupaymentmethodstrans found with id: {}" , lupaymentmethodstransId);
            throw new EntityNotFoundException(String.valueOf(lupaymentmethodstransId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "abTransactionManager")
    @Override
    public Page<LuPaymentMethodsTrans> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all lupaymentmethodstranss");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "abTransactionManager")
    @Override
    public Page<LuPaymentMethodsTrans> findAll(Pageable pageable) {
        LOGGER.debug("Finding all lupaymentmethodstranss");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "abTransactionManager")
    @Override
    public LuPaymentMethodsTrans findById(LuPaymentMethodsTransId id) throws EntityNotFoundException {
        LOGGER.debug("Finding lupaymentmethodstrans by id: {}" , id);
        LuPaymentMethodsTrans lupaymentmethodstrans=this.wmGenericDao.findById(id);
        if(lupaymentmethodstrans==null){
            LOGGER.debug("No lupaymentmethodstrans found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return lupaymentmethodstrans;
    }
    @Transactional(rollbackFor = EntityNotFoundException.class, value = "abTransactionManager")
    @Override
    public LuPaymentMethodsTrans update(LuPaymentMethodsTrans updated) throws EntityNotFoundException {
        LOGGER.debug("Updating lupaymentmethodstrans with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((LuPaymentMethodsTransId)updated.getId());
    }

    @Transactional(readOnly = true, value = "abTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


