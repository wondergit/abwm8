/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;

/**
 * Service object for domain model class WkPermHasLanguage.
 * @see com.pointswell.ab.ab.WkPermHasLanguage
 */

public interface WkPermHasLanguageService {
   /**
	 * Creates a new wkpermhaslanguage.
	 * 
	 * @param created
	 *            The information of the created wkpermhaslanguage.
	 * @return The created wkpermhaslanguage.
	 */
	public WkPermHasLanguage create(WkPermHasLanguage created);

	/**
	 * Deletes a wkpermhaslanguage.
	 * 
	 * @param wkpermhaslanguageId
	 *            The id of the deleted wkpermhaslanguage.
	 * @return The deleted wkpermhaslanguage.
	 * @throws EntityNotFoundException
	 *             if no wkpermhaslanguage is found with the given id.
	 */
	public WkPermHasLanguage delete(WkPermHasLanguageId wkpermhaslanguageId) throws EntityNotFoundException;

	/**
	 * Finds all wkpermhaslanguages.
	 * 
	 * @return A list of wkpermhaslanguages.
	 */
	public Page<WkPermHasLanguage> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<WkPermHasLanguage> findAll(Pageable pageable);
	
	/**
	 * Finds wkpermhaslanguage by id.
	 * 
	 * @param id
	 *            The id of the wanted wkpermhaslanguage.
	 * @return The found wkpermhaslanguage. If no wkpermhaslanguage is found, this method returns
	 *         null.
	 */
	public WkPermHasLanguage findById(WkPermHasLanguageId id) throws EntityNotFoundException;
	/**
	 * Updates the information of a wkpermhaslanguage.
	 * 
	 * @param updated
	 *            The information of the updated wkpermhaslanguage.
	 * @return The updated wkpermhaslanguage.
	 * @throws EntityNotFoundException
	 *             if no wkpermhaslanguage is found with given id.
	 */
	public WkPermHasLanguage update(WkPermHasLanguage updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the wkpermhaslanguages in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the wkpermhaslanguage.
	 */

	public long countAll();


    public Page<WkPermHasLanguage> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}

