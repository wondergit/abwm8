/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;

/**
 * Service object for domain model class WkTitles.
 * @see com.pointswell.ab.ab.WkTitles
 */

public interface WkTitlesService {
   /**
	 * Creates a new wktitles.
	 * 
	 * @param created
	 *            The information of the created wktitles.
	 * @return The created wktitles.
	 */
	public WkTitles create(WkTitles created);

	/**
	 * Deletes a wktitles.
	 * 
	 * @param wktitlesId
	 *            The id of the deleted wktitles.
	 * @return The deleted wktitles.
	 * @throws EntityNotFoundException
	 *             if no wktitles is found with the given id.
	 */
	public WkTitles delete(Integer wktitlesId) throws EntityNotFoundException;

	/**
	 * Finds all wktitless.
	 * 
	 * @return A list of wktitless.
	 */
	public Page<WkTitles> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<WkTitles> findAll(Pageable pageable);
	
	/**
	 * Finds wktitles by id.
	 * 
	 * @param id
	 *            The id of the wanted wktitles.
	 * @return The found wktitles. If no wktitles is found, this method returns
	 *         null.
	 */
	public WkTitles findById(Integer id) throws EntityNotFoundException;
	/**
	 * Updates the information of a wktitles.
	 * 
	 * @param updated
	 *            The information of the updated wktitles.
	 * @return The updated wktitles.
	 * @throws EntityNotFoundException
	 *             if no wktitles is found with given id.
	 */
	public WkTitles update(WkTitles updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the wktitless in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the wktitles.
	 */

	public long countAll();


    public Page<WkTitles> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}

