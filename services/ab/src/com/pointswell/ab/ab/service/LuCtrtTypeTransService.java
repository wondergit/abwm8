/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;

/**
 * Service object for domain model class LuCtrtTypeTrans.
 * @see com.pointswell.ab.ab.LuCtrtTypeTrans
 */

public interface LuCtrtTypeTransService {
   /**
	 * Creates a new luctrttypetrans.
	 * 
	 * @param created
	 *            The information of the created luctrttypetrans.
	 * @return The created luctrttypetrans.
	 */
	public LuCtrtTypeTrans create(LuCtrtTypeTrans created);

	/**
	 * Deletes a luctrttypetrans.
	 * 
	 * @param luctrttypetransId
	 *            The id of the deleted luctrttypetrans.
	 * @return The deleted luctrttypetrans.
	 * @throws EntityNotFoundException
	 *             if no luctrttypetrans is found with the given id.
	 */
	public LuCtrtTypeTrans delete(LuCtrtTypeTransId luctrttypetransId) throws EntityNotFoundException;

	/**
	 * Finds all luctrttypetranss.
	 * 
	 * @return A list of luctrttypetranss.
	 */
	public Page<LuCtrtTypeTrans> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<LuCtrtTypeTrans> findAll(Pageable pageable);
	
	/**
	 * Finds luctrttypetrans by id.
	 * 
	 * @param id
	 *            The id of the wanted luctrttypetrans.
	 * @return The found luctrttypetrans. If no luctrttypetrans is found, this method returns
	 *         null.
	 */
	public LuCtrtTypeTrans findById(LuCtrtTypeTransId id) throws EntityNotFoundException;
	/**
	 * Updates the information of a luctrttypetrans.
	 * 
	 * @param updated
	 *            The information of the updated luctrttypetrans.
	 * @return The updated luctrttypetrans.
	 * @throws EntityNotFoundException
	 *             if no luctrttypetrans is found with given id.
	 */
	public LuCtrtTypeTrans update(LuCtrtTypeTrans updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the luctrttypetranss in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the luctrttypetrans.
	 */

	public long countAll();


    public Page<LuCtrtTypeTrans> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}

