/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;

/**
 * Service object for domain model class LuDelTypeTrans.
 * @see com.pointswell.ab.ab.LuDelTypeTrans
 */

public interface LuDelTypeTransService {
   /**
	 * Creates a new ludeltypetrans.
	 * 
	 * @param created
	 *            The information of the created ludeltypetrans.
	 * @return The created ludeltypetrans.
	 */
	public LuDelTypeTrans create(LuDelTypeTrans created);

	/**
	 * Deletes a ludeltypetrans.
	 * 
	 * @param ludeltypetransId
	 *            The id of the deleted ludeltypetrans.
	 * @return The deleted ludeltypetrans.
	 * @throws EntityNotFoundException
	 *             if no ludeltypetrans is found with the given id.
	 */
	public LuDelTypeTrans delete(LuDelTypeTransId ludeltypetransId) throws EntityNotFoundException;

	/**
	 * Finds all ludeltypetranss.
	 * 
	 * @return A list of ludeltypetranss.
	 */
	public Page<LuDelTypeTrans> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<LuDelTypeTrans> findAll(Pageable pageable);
	
	/**
	 * Finds ludeltypetrans by id.
	 * 
	 * @param id
	 *            The id of the wanted ludeltypetrans.
	 * @return The found ludeltypetrans. If no ludeltypetrans is found, this method returns
	 *         null.
	 */
	public LuDelTypeTrans findById(LuDelTypeTransId id) throws EntityNotFoundException;
	/**
	 * Updates the information of a ludeltypetrans.
	 * 
	 * @param updated
	 *            The information of the updated ludeltypetrans.
	 * @return The updated ludeltypetrans.
	 * @throws EntityNotFoundException
	 *             if no ludeltypetrans is found with given id.
	 */
	public LuDelTypeTrans update(LuDelTypeTrans updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the ludeltypetranss in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the ludeltypetrans.
	 */

	public long countAll();


    public Page<LuDelTypeTrans> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}

