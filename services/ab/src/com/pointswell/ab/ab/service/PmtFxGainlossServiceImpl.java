/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;


/**
 * ServiceImpl object for domain model class PmtFxGainloss.
 * @see com.pointswell.ab.ab.PmtFxGainloss
 */
@Service("ab.PmtFxGainlossService")
public class PmtFxGainlossServiceImpl implements PmtFxGainlossService {


    private static final Logger LOGGER = LoggerFactory.getLogger(PmtFxGainlossServiceImpl.class);

    @Autowired
    @Qualifier("ab.PmtFxGainlossDao")
    private WMGenericDao<PmtFxGainloss, Integer> wmGenericDao;
    public void setWMGenericDao(WMGenericDao<PmtFxGainloss, Integer> wmGenericDao){
        this.wmGenericDao = wmGenericDao;
    }
     @Transactional(readOnly = true, value = "abTransactionManager")
     public Page<PmtFxGainloss> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable){
          LOGGER.debug("Fetching all associated");
          return this.wmGenericDao.getAssociatedObjects(value, entityName, key, pageable);
     }

    @Transactional(value = "abTransactionManager")
    @Override
    public PmtFxGainloss create(PmtFxGainloss pmtfxgainloss) {
        LOGGER.debug("Creating a new pmtfxgainloss with information: {}" , pmtfxgainloss);
        return this.wmGenericDao.create(pmtfxgainloss);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "abTransactionManager")
    @Override
    public PmtFxGainloss delete(Integer pmtfxgainlossId) throws EntityNotFoundException {
        LOGGER.debug("Deleting pmtfxgainloss with id: {}" , pmtfxgainlossId);
        PmtFxGainloss deleted = this.wmGenericDao.findById(pmtfxgainlossId);
        if (deleted == null) {
            LOGGER.debug("No pmtfxgainloss found with id: {}" , pmtfxgainlossId);
            throw new EntityNotFoundException(String.valueOf(pmtfxgainlossId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "abTransactionManager")
    @Override
    public Page<PmtFxGainloss> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all pmtfxgainlosss");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "abTransactionManager")
    @Override
    public Page<PmtFxGainloss> findAll(Pageable pageable) {
        LOGGER.debug("Finding all pmtfxgainlosss");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "abTransactionManager")
    @Override
    public PmtFxGainloss findById(Integer id) throws EntityNotFoundException {
        LOGGER.debug("Finding pmtfxgainloss by id: {}" , id);
        PmtFxGainloss pmtfxgainloss=this.wmGenericDao.findById(id);
        if(pmtfxgainloss==null){
            LOGGER.debug("No pmtfxgainloss found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return pmtfxgainloss;
    }
    @Transactional(rollbackFor = EntityNotFoundException.class, value = "abTransactionManager")
    @Override
    public PmtFxGainloss update(PmtFxGainloss updated) throws EntityNotFoundException {
        LOGGER.debug("Updating pmtfxgainloss with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((Integer)updated.getPmtFxGlid());
    }

    @Transactional(readOnly = true, value = "abTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


