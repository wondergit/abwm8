/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;


/**
 * ServiceImpl object for domain model class LuCtrtStatus.
 * @see com.pointswell.ab.ab.LuCtrtStatus
 */
@Service("ab.LuCtrtStatusService")
public class LuCtrtStatusServiceImpl implements LuCtrtStatusService {


    private static final Logger LOGGER = LoggerFactory.getLogger(LuCtrtStatusServiceImpl.class);

    @Autowired
    @Qualifier("ab.LuCtrtStatusDao")
    private WMGenericDao<LuCtrtStatus, Integer> wmGenericDao;
    public void setWMGenericDao(WMGenericDao<LuCtrtStatus, Integer> wmGenericDao){
        this.wmGenericDao = wmGenericDao;
    }
     @Transactional(readOnly = true, value = "abTransactionManager")
     public Page<LuCtrtStatus> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable){
          LOGGER.debug("Fetching all associated");
          return this.wmGenericDao.getAssociatedObjects(value, entityName, key, pageable);
     }

    @Transactional(value = "abTransactionManager")
    @Override
    public LuCtrtStatus create(LuCtrtStatus luctrtstatus) {
        LOGGER.debug("Creating a new luctrtstatus with information: {}" , luctrtstatus);
        return this.wmGenericDao.create(luctrtstatus);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "abTransactionManager")
    @Override
    public LuCtrtStatus delete(Integer luctrtstatusId) throws EntityNotFoundException {
        LOGGER.debug("Deleting luctrtstatus with id: {}" , luctrtstatusId);
        LuCtrtStatus deleted = this.wmGenericDao.findById(luctrtstatusId);
        if (deleted == null) {
            LOGGER.debug("No luctrtstatus found with id: {}" , luctrtstatusId);
            throw new EntityNotFoundException(String.valueOf(luctrtstatusId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "abTransactionManager")
    @Override
    public Page<LuCtrtStatus> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all luctrtstatuss");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "abTransactionManager")
    @Override
    public Page<LuCtrtStatus> findAll(Pageable pageable) {
        LOGGER.debug("Finding all luctrtstatuss");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "abTransactionManager")
    @Override
    public LuCtrtStatus findById(Integer id) throws EntityNotFoundException {
        LOGGER.debug("Finding luctrtstatus by id: {}" , id);
        LuCtrtStatus luctrtstatus=this.wmGenericDao.findById(id);
        if(luctrtstatus==null){
            LOGGER.debug("No luctrtstatus found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return luctrtstatus;
    }
    @Transactional(rollbackFor = EntityNotFoundException.class, value = "abTransactionManager")
    @Override
    public LuCtrtStatus update(LuCtrtStatus updated) throws EntityNotFoundException {
        LOGGER.debug("Updating luctrtstatus with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((Integer)updated.getLuCtrtStatusid());
    }

    @Transactional(readOnly = true, value = "abTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


