/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;

/**
 * Service object for domain model class SysUsers.
 * @see com.pointswell.ab.ab.SysUsers
 */

public interface SysUsersService {
   /**
	 * Creates a new sysusers.
	 * 
	 * @param created
	 *            The information of the created sysusers.
	 * @return The created sysusers.
	 */
	public SysUsers create(SysUsers created);

	/**
	 * Deletes a sysusers.
	 * 
	 * @param sysusersId
	 *            The id of the deleted sysusers.
	 * @return The deleted sysusers.
	 * @throws EntityNotFoundException
	 *             if no sysusers is found with the given id.
	 */
	public SysUsers delete(Integer sysusersId) throws EntityNotFoundException;

	/**
	 * Finds all sysuserss.
	 * 
	 * @return A list of sysuserss.
	 */
	public Page<SysUsers> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<SysUsers> findAll(Pageable pageable);
	
	/**
	 * Finds sysusers by id.
	 * 
	 * @param id
	 *            The id of the wanted sysusers.
	 * @return The found sysusers. If no sysusers is found, this method returns
	 *         null.
	 */
	public SysUsers findById(Integer id) throws EntityNotFoundException;
	/**
	 * Updates the information of a sysusers.
	 * 
	 * @param updated
	 *            The information of the updated sysusers.
	 * @return The updated sysusers.
	 * @throws EntityNotFoundException
	 *             if no sysusers is found with given id.
	 */
	public SysUsers update(SysUsers updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the sysuserss in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the sysusers.
	 */

	public long countAll();


    public Page<SysUsers> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}

