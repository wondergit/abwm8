/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;

/**
 * Service object for domain model class WkHasGenre.
 * @see com.pointswell.ab.ab.WkHasGenre
 */

public interface WkHasGenreService {
   /**
	 * Creates a new wkhasgenre.
	 * 
	 * @param created
	 *            The information of the created wkhasgenre.
	 * @return The created wkhasgenre.
	 */
	public WkHasGenre create(WkHasGenre created);

	/**
	 * Deletes a wkhasgenre.
	 * 
	 * @param wkhasgenreId
	 *            The id of the deleted wkhasgenre.
	 * @return The deleted wkhasgenre.
	 * @throws EntityNotFoundException
	 *             if no wkhasgenre is found with the given id.
	 */
	public WkHasGenre delete(WkHasGenreId wkhasgenreId) throws EntityNotFoundException;

	/**
	 * Finds all wkhasgenres.
	 * 
	 * @return A list of wkhasgenres.
	 */
	public Page<WkHasGenre> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<WkHasGenre> findAll(Pageable pageable);
	
	/**
	 * Finds wkhasgenre by id.
	 * 
	 * @param id
	 *            The id of the wanted wkhasgenre.
	 * @return The found wkhasgenre. If no wkhasgenre is found, this method returns
	 *         null.
	 */
	public WkHasGenre findById(WkHasGenreId id) throws EntityNotFoundException;
	/**
	 * Updates the information of a wkhasgenre.
	 * 
	 * @param updated
	 *            The information of the updated wkhasgenre.
	 * @return The updated wkhasgenre.
	 * @throws EntityNotFoundException
	 *             if no wkhasgenre is found with given id.
	 */
	public WkHasGenre update(WkHasGenre updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the wkhasgenres in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the wkhasgenre.
	 */

	public long countAll();


    public Page<WkHasGenre> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}

