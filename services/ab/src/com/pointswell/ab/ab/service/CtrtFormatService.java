/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;

/**
 * Service object for domain model class CtrtFormat.
 * @see com.pointswell.ab.ab.CtrtFormat
 */

public interface CtrtFormatService {
   /**
	 * Creates a new ctrtformat.
	 * 
	 * @param created
	 *            The information of the created ctrtformat.
	 * @return The created ctrtformat.
	 */
	public CtrtFormat create(CtrtFormat created);

	/**
	 * Deletes a ctrtformat.
	 * 
	 * @param ctrtformatId
	 *            The id of the deleted ctrtformat.
	 * @return The deleted ctrtformat.
	 * @throws EntityNotFoundException
	 *             if no ctrtformat is found with the given id.
	 */
	public CtrtFormat delete(Integer ctrtformatId) throws EntityNotFoundException;

	/**
	 * Finds all ctrtformats.
	 * 
	 * @return A list of ctrtformats.
	 */
	public Page<CtrtFormat> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<CtrtFormat> findAll(Pageable pageable);
	
	/**
	 * Finds ctrtformat by id.
	 * 
	 * @param id
	 *            The id of the wanted ctrtformat.
	 * @return The found ctrtformat. If no ctrtformat is found, this method returns
	 *         null.
	 */
	public CtrtFormat findById(Integer id) throws EntityNotFoundException;
	/**
	 * Updates the information of a ctrtformat.
	 * 
	 * @param updated
	 *            The information of the updated ctrtformat.
	 * @return The updated ctrtformat.
	 * @throws EntityNotFoundException
	 *             if no ctrtformat is found with given id.
	 */
	public CtrtFormat update(CtrtFormat updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the ctrtformats in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the ctrtformat.
	 */

	public long countAll();


    public Page<CtrtFormat> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}

