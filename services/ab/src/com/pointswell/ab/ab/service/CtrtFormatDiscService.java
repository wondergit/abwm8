/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;

/**
 * Service object for domain model class CtrtFormatDisc.
 * @see com.pointswell.ab.ab.CtrtFormatDisc
 */

public interface CtrtFormatDiscService {
   /**
	 * Creates a new ctrtformatdisc.
	 * 
	 * @param created
	 *            The information of the created ctrtformatdisc.
	 * @return The created ctrtformatdisc.
	 */
	public CtrtFormatDisc create(CtrtFormatDisc created);

	/**
	 * Deletes a ctrtformatdisc.
	 * 
	 * @param ctrtformatdiscId
	 *            The id of the deleted ctrtformatdisc.
	 * @return The deleted ctrtformatdisc.
	 * @throws EntityNotFoundException
	 *             if no ctrtformatdisc is found with the given id.
	 */
	public CtrtFormatDisc delete(Integer ctrtformatdiscId) throws EntityNotFoundException;

	/**
	 * Finds all ctrtformatdiscs.
	 * 
	 * @return A list of ctrtformatdiscs.
	 */
	public Page<CtrtFormatDisc> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<CtrtFormatDisc> findAll(Pageable pageable);
	
	/**
	 * Finds ctrtformatdisc by id.
	 * 
	 * @param id
	 *            The id of the wanted ctrtformatdisc.
	 * @return The found ctrtformatdisc. If no ctrtformatdisc is found, this method returns
	 *         null.
	 */
	public CtrtFormatDisc findById(Integer id) throws EntityNotFoundException;
	/**
	 * Updates the information of a ctrtformatdisc.
	 * 
	 * @param updated
	 *            The information of the updated ctrtformatdisc.
	 * @return The updated ctrtformatdisc.
	 * @throws EntityNotFoundException
	 *             if no ctrtformatdisc is found with given id.
	 */
	public CtrtFormatDisc update(CtrtFormatDisc updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the ctrtformatdiscs in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the ctrtformatdisc.
	 */

	public long countAll();


    public Page<CtrtFormatDisc> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}

