/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;


/**
 * ServiceImpl object for domain model class LuResponseStatus.
 * @see com.pointswell.ab.ab.LuResponseStatus
 */
@Service("ab.LuResponseStatusService")
public class LuResponseStatusServiceImpl implements LuResponseStatusService {


    private static final Logger LOGGER = LoggerFactory.getLogger(LuResponseStatusServiceImpl.class);

    @Autowired
    @Qualifier("ab.LuResponseStatusDao")
    private WMGenericDao<LuResponseStatus, Integer> wmGenericDao;
    public void setWMGenericDao(WMGenericDao<LuResponseStatus, Integer> wmGenericDao){
        this.wmGenericDao = wmGenericDao;
    }
     @Transactional(readOnly = true, value = "abTransactionManager")
     public Page<LuResponseStatus> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable){
          LOGGER.debug("Fetching all associated");
          return this.wmGenericDao.getAssociatedObjects(value, entityName, key, pageable);
     }

    @Transactional(value = "abTransactionManager")
    @Override
    public LuResponseStatus create(LuResponseStatus luresponsestatus) {
        LOGGER.debug("Creating a new luresponsestatus with information: {}" , luresponsestatus);
        return this.wmGenericDao.create(luresponsestatus);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "abTransactionManager")
    @Override
    public LuResponseStatus delete(Integer luresponsestatusId) throws EntityNotFoundException {
        LOGGER.debug("Deleting luresponsestatus with id: {}" , luresponsestatusId);
        LuResponseStatus deleted = this.wmGenericDao.findById(luresponsestatusId);
        if (deleted == null) {
            LOGGER.debug("No luresponsestatus found with id: {}" , luresponsestatusId);
            throw new EntityNotFoundException(String.valueOf(luresponsestatusId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "abTransactionManager")
    @Override
    public Page<LuResponseStatus> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all luresponsestatuss");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "abTransactionManager")
    @Override
    public Page<LuResponseStatus> findAll(Pageable pageable) {
        LOGGER.debug("Finding all luresponsestatuss");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "abTransactionManager")
    @Override
    public LuResponseStatus findById(Integer id) throws EntityNotFoundException {
        LOGGER.debug("Finding luresponsestatus by id: {}" , id);
        LuResponseStatus luresponsestatus=this.wmGenericDao.findById(id);
        if(luresponsestatus==null){
            LOGGER.debug("No luresponsestatus found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return luresponsestatus;
    }
    @Transactional(rollbackFor = EntityNotFoundException.class, value = "abTransactionManager")
    @Override
    public LuResponseStatus update(LuResponseStatus updated) throws EntityNotFoundException {
        LOGGER.debug("Updating luresponsestatus with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((Integer)updated.getLuResponseStatusid());
    }

    @Transactional(readOnly = true, value = "abTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


