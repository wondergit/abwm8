/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;

/**
 * Service object for domain model class PmtContactAcc.
 * @see com.pointswell.ab.ab.PmtContactAcc
 */

public interface PmtContactAccService {
   /**
	 * Creates a new pmtcontactacc.
	 * 
	 * @param created
	 *            The information of the created pmtcontactacc.
	 * @return The created pmtcontactacc.
	 */
	public PmtContactAcc create(PmtContactAcc created);

	/**
	 * Deletes a pmtcontactacc.
	 * 
	 * @param pmtcontactaccId
	 *            The id of the deleted pmtcontactacc.
	 * @return The deleted pmtcontactacc.
	 * @throws EntityNotFoundException
	 *             if no pmtcontactacc is found with the given id.
	 */
	public PmtContactAcc delete(Integer pmtcontactaccId) throws EntityNotFoundException;

	/**
	 * Finds all pmtcontactaccs.
	 * 
	 * @return A list of pmtcontactaccs.
	 */
	public Page<PmtContactAcc> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<PmtContactAcc> findAll(Pageable pageable);
	
	/**
	 * Finds pmtcontactacc by id.
	 * 
	 * @param id
	 *            The id of the wanted pmtcontactacc.
	 * @return The found pmtcontactacc. If no pmtcontactacc is found, this method returns
	 *         null.
	 */
	public PmtContactAcc findById(Integer id) throws EntityNotFoundException;
	/**
	 * Updates the information of a pmtcontactacc.
	 * 
	 * @param updated
	 *            The information of the updated pmtcontactacc.
	 * @return The updated pmtcontactacc.
	 * @throws EntityNotFoundException
	 *             if no pmtcontactacc is found with given id.
	 */
	public PmtContactAcc update(PmtContactAcc updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the pmtcontactaccs in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the pmtcontactacc.
	 */

	public long countAll();


    public Page<PmtContactAcc> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}

