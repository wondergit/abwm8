/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;

/**
 * Service object for domain model class CntPoa.
 * @see com.pointswell.ab.ab.CntPoa
 */

public interface CntPoaService {
   /**
	 * Creates a new cntpoa.
	 * 
	 * @param created
	 *            The information of the created cntpoa.
	 * @return The created cntpoa.
	 */
	public CntPoa create(CntPoa created);

	/**
	 * Deletes a cntpoa.
	 * 
	 * @param cntpoaId
	 *            The id of the deleted cntpoa.
	 * @return The deleted cntpoa.
	 * @throws EntityNotFoundException
	 *             if no cntpoa is found with the given id.
	 */
	public CntPoa delete(Integer cntpoaId) throws EntityNotFoundException;

	/**
	 * Finds all cntpoas.
	 * 
	 * @return A list of cntpoas.
	 */
	public Page<CntPoa> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<CntPoa> findAll(Pageable pageable);
	
	/**
	 * Finds cntpoa by id.
	 * 
	 * @param id
	 *            The id of the wanted cntpoa.
	 * @return The found cntpoa. If no cntpoa is found, this method returns
	 *         null.
	 */
	public CntPoa findById(Integer id) throws EntityNotFoundException;
	/**
	 * Updates the information of a cntpoa.
	 * 
	 * @param updated
	 *            The information of the updated cntpoa.
	 * @return The updated cntpoa.
	 * @throws EntityNotFoundException
	 *             if no cntpoa is found with given id.
	 */
	public CntPoa update(CntPoa updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the cntpoas in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the cntpoa.
	 */

	public long countAll();


    public Page<CntPoa> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}

