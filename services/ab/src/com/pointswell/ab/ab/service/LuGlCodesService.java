/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;

/**
 * Service object for domain model class LuGlCodes.
 * @see com.pointswell.ab.ab.LuGlCodes
 */

public interface LuGlCodesService {
   /**
	 * Creates a new luglcodes.
	 * 
	 * @param created
	 *            The information of the created luglcodes.
	 * @return The created luglcodes.
	 */
	public LuGlCodes create(LuGlCodes created);

	/**
	 * Deletes a luglcodes.
	 * 
	 * @param luglcodesId
	 *            The id of the deleted luglcodes.
	 * @return The deleted luglcodes.
	 * @throws EntityNotFoundException
	 *             if no luglcodes is found with the given id.
	 */
	public LuGlCodes delete(Integer luglcodesId) throws EntityNotFoundException;

	/**
	 * Finds all luglcodess.
	 * 
	 * @return A list of luglcodess.
	 */
	public Page<LuGlCodes> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<LuGlCodes> findAll(Pageable pageable);
	
	/**
	 * Finds luglcodes by id.
	 * 
	 * @param id
	 *            The id of the wanted luglcodes.
	 * @return The found luglcodes. If no luglcodes is found, this method returns
	 *         null.
	 */
	public LuGlCodes findById(Integer id) throws EntityNotFoundException;
	/**
	 * Updates the information of a luglcodes.
	 * 
	 * @param updated
	 *            The information of the updated luglcodes.
	 * @return The updated luglcodes.
	 * @throws EntityNotFoundException
	 *             if no luglcodes is found with given id.
	 */
	public LuGlCodes update(LuGlCodes updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the luglcodess in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the luglcodes.
	 */

	public long countAll();


    public Page<LuGlCodes> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}

