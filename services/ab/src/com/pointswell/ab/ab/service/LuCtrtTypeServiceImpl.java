/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.pointswell.ab.ab.*;


/**
 * ServiceImpl object for domain model class LuCtrtType.
 * @see com.pointswell.ab.ab.LuCtrtType
 */
@Service("ab.LuCtrtTypeService")
public class LuCtrtTypeServiceImpl implements LuCtrtTypeService {


    private static final Logger LOGGER = LoggerFactory.getLogger(LuCtrtTypeServiceImpl.class);

    @Autowired
    @Qualifier("ab.LuCtrtTypeDao")
    private WMGenericDao<LuCtrtType, Integer> wmGenericDao;
    public void setWMGenericDao(WMGenericDao<LuCtrtType, Integer> wmGenericDao){
        this.wmGenericDao = wmGenericDao;
    }
     @Transactional(readOnly = true, value = "abTransactionManager")
     public Page<LuCtrtType> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable){
          LOGGER.debug("Fetching all associated");
          return this.wmGenericDao.getAssociatedObjects(value, entityName, key, pageable);
     }

    @Transactional(value = "abTransactionManager")
    @Override
    public LuCtrtType create(LuCtrtType luctrttype) {
        LOGGER.debug("Creating a new luctrttype with information: {}" , luctrttype);
        return this.wmGenericDao.create(luctrttype);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "abTransactionManager")
    @Override
    public LuCtrtType delete(Integer luctrttypeId) throws EntityNotFoundException {
        LOGGER.debug("Deleting luctrttype with id: {}" , luctrttypeId);
        LuCtrtType deleted = this.wmGenericDao.findById(luctrttypeId);
        if (deleted == null) {
            LOGGER.debug("No luctrttype found with id: {}" , luctrttypeId);
            throw new EntityNotFoundException(String.valueOf(luctrttypeId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "abTransactionManager")
    @Override
    public Page<LuCtrtType> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all luctrttypes");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "abTransactionManager")
    @Override
    public Page<LuCtrtType> findAll(Pageable pageable) {
        LOGGER.debug("Finding all luctrttypes");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "abTransactionManager")
    @Override
    public LuCtrtType findById(Integer id) throws EntityNotFoundException {
        LOGGER.debug("Finding luctrttype by id: {}" , id);
        LuCtrtType luctrttype=this.wmGenericDao.findById(id);
        if(luctrttype==null){
            LOGGER.debug("No luctrttype found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return luctrttype;
    }
    @Transactional(rollbackFor = EntityNotFoundException.class, value = "abTransactionManager")
    @Override
    public LuCtrtType update(LuCtrtType updated) throws EntityNotFoundException {
        LOGGER.debug("Updating luctrttype with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((Integer)updated.getLuCtrtTypeid());
    }

    @Transactional(readOnly = true, value = "abTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


