/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import javax.persistence.PrimaryKeyJoinColumn;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import java.util.Arrays;

import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.UniqueConstraint;




/**
 * WkHasContributor generated by hbm2java
 */
@Entity
@Table(name="wk_has_contributor"
)
public class WkHasContributor  implements java.io.Serializable
 {


private WkHasContributorId id;
private Date xcreateddate;
private String xcreatedby;
private Date xmodifieddate;
private String xmodifiedby;
private String xaudit;
private Boolean feepayable;
private BigDecimal contributorFee;
private Integer sysProcessLogid;
private Contact contact;
private LuContributionType luContributionType;
private LuCurrency luCurrency;
private Tenant tenant;
private Work work;

    public WkHasContributor() {
    }



     @EmbeddedId

    

    @AttributeOverrides( {
        @AttributeOverride(name="workid", column=@Column(name="workid", nullable=false) ), 
        @AttributeOverride(name="contactid", column=@Column(name="contactid", nullable=false) ) } )
    public WkHasContributorId getId() {
        return this.id;
    }
    
    public void setId(WkHasContributorId id) {
        this.id = id;
    }

    @Temporal(TemporalType.TIMESTAMP)

    @Column(name="xcreateddate", nullable=false, length=19)
    public Date getXcreateddate() {
        return this.xcreateddate;
    }
    
    public void setXcreateddate(Date xcreateddate) {
        this.xcreateddate = xcreateddate;
    }

    

    @Column(name="xcreatedby", nullable=false, length=45)
    public String getXcreatedby() {
        return this.xcreatedby;
    }
    
    public void setXcreatedby(String xcreatedby) {
        this.xcreatedby = xcreatedby;
    }

    @Temporal(TemporalType.TIMESTAMP)

    @Column(name="xmodifieddate", length=19)
    public Date getXmodifieddate() {
        return this.xmodifieddate;
    }
    
    public void setXmodifieddate(Date xmodifieddate) {
        this.xmodifieddate = xmodifieddate;
    }

    

    @Column(name="xmodifiedby", length=45)
    public String getXmodifiedby() {
        return this.xmodifiedby;
    }
    
    public void setXmodifiedby(String xmodifiedby) {
        this.xmodifiedby = xmodifiedby;
    }

    

    @Column(name="xaudit")
    public String getXaudit() {
        return this.xaudit;
    }
    
    public void setXaudit(String xaudit) {
        this.xaudit = xaudit;
    }

    

    @Column(name="feepayable")
    public Boolean getFeepayable() {
        return this.feepayable;
    }
    
    public void setFeepayable(Boolean feepayable) {
        this.feepayable = feepayable;
    }

    

    @Column(name="contributor_fee", precision=8)
    public BigDecimal getContributorFee() {
        return this.contributorFee;
    }
    
    public void setContributorFee(BigDecimal contributorFee) {
        this.contributorFee = contributorFee;
    }

    

    @Column(name="sys_process_logid")
    public Integer getSysProcessLogid() {
        return this.sysProcessLogid;
    }
    
    public void setSysProcessLogid(Integer sysProcessLogid) {
        this.sysProcessLogid = sysProcessLogid;
    }

@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="contactid", nullable=false, insertable=false, updatable=false)
    public Contact getContact() {
        return this.contact;
    }
    
    public void setContact(Contact contact) {
        this.contact = contact;
    }

@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="lu_contributiontypeid", nullable=false)
    public LuContributionType getLuContributionType() {
        return this.luContributionType;
    }
    
    public void setLuContributionType(LuContributionType luContributionType) {
        this.luContributionType = luContributionType;
    }

@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="contributor_fee_curr", nullable=false)
    public LuCurrency getLuCurrency() {
        return this.luCurrency;
    }
    
    public void setLuCurrency(LuCurrency luCurrency) {
        this.luCurrency = luCurrency;
    }

@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="tenantid", nullable=false)
    public Tenant getTenant() {
        return this.tenant;
    }
    
    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="workid", nullable=false, insertable=false, updatable=false)
    public Work getWork() {
        return this.work;
    }
    
    public void setWork(Work work) {
        this.work = work;
    }



public boolean equals(Object o) {
         if (this == o)
         return true;
		 if ( (o == null ) )
		 return false;
		 if ( !(o instanceof WkHasContributor) )
		 return false;

		 WkHasContributor that = ( WkHasContributor ) o;

		 return ( (this.getId()==that.getId()) || ( this.getId()!=null && that.getId()!=null && this.getId().equals(that.getId()) ) );

   }

   public int hashCode() {
         int result = 17;

         result = 37 * result + ( getId() == null ? 0 : this.getId().hashCode() );

         return result;
     }


}

