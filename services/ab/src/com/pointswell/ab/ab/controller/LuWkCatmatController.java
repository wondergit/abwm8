/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import com.pointswell.ab.ab.service.LuWkCatmatService;
import com.pointswell.ab.ab.service.WkCatalogueService;
import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.TypeMismatchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.util.WMMultipartUtils;
import com.wavemaker.runtime.util.WMRuntimeUtils;
import com.wordnik.swagger.annotations.*;
import com.pointswell.ab.ab.*;
import com.pointswell.ab.ab.service.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class LuWkCatmat.
 * @see com.pointswell.ab.ab.LuWkCatmat
 */
@RestController(value = "Ab.LuWkCatmatController")
@RequestMapping("/ab/LuWkCatmat")
@Api(description = "Exposes APIs to work with LuWkCatmat resource.", value = "LuWkCatmatController")
public class LuWkCatmatController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LuWkCatmatController.class);

    @Autowired
    @Qualifier("ab.LuWkCatmatService")
    private LuWkCatmatService luWkCatmatService;

    @Autowired
    @Qualifier("ab.WkCatalogueService")
    private WkCatalogueService wkCatalogueService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of LuWkCatmat instances matching the search criteria.")
    public Page<LuWkCatmat> findLuWkCatmats(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering LuWkCatmats list");
        return luWkCatmatService.findAll(queryFilters, pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of LuWkCatmat instances.")
    public Page<LuWkCatmat> getLuWkCatmats(Pageable pageable) {
        LOGGER.debug("Rendering LuWkCatmats list");
        return luWkCatmatService.findAll(pageable);
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the LuWkCatmat instance associated with the given id.")
    public LuWkCatmat getLuWkCatmat(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting LuWkCatmat with id: {}", id);
        LuWkCatmat instance = luWkCatmatService.findById(id);
        LOGGER.debug("LuWkCatmat details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes the LuWkCatmat instance associated with the given id.")
    public boolean deleteLuWkCatmat(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting LuWkCatmat with id: {}", id);
        LuWkCatmat deleted = luWkCatmatService.delete(id);
        return deleted != null;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates the LuWkCatmat instance associated with the given id.")
    public LuWkCatmat editLuWkCatmat(@PathVariable("id") Integer id, @RequestBody LuWkCatmat instance) throws EntityNotFoundException {
        LOGGER.debug("Editing LuWkCatmat with id: {}", instance.getLuWkCatmatid());
        instance.setLuWkCatmatid(id);
        instance = luWkCatmatService.update(instance);
        LOGGER.debug("LuWkCatmat details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}/wkCatalogues", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the wkCatalogues instance associated with the given id.")
    public Page<WkCatalogue> findAssociatedwkCatalogues(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated wkCatalogues");
        return wkCatalogueService.findAssociatedValues(id, "luWkCatmat", "luWkCatmatid", pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ApiOperation(value = "Creates a new LuWkCatmat instance.")
    public LuWkCatmat createLuWkCatmat(@RequestBody LuWkCatmat instance) {
        LOGGER.debug("Create LuWkCatmat with information: {}", instance);
        instance = luWkCatmatService.create(instance);
        LOGGER.debug("Created LuWkCatmat with information: {}", instance);
        return instance;
    }

    /**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
    protected void setLuWkCatmatService(LuWkCatmatService service) {
        this.luWkCatmatService = service;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of LuWkCatmat instances.")
    public Long countAllLuWkCatmats() {
        LOGGER.debug("counting LuWkCatmats");
        Long count = luWkCatmatService.countAll();
        return count;
    }
}
