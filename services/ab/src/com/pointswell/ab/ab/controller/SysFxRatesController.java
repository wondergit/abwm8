/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import com.pointswell.ab.ab.service.SysFxRatesService;
import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.TypeMismatchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.util.WMMultipartUtils;
import com.wavemaker.runtime.util.WMRuntimeUtils;
import com.wordnik.swagger.annotations.*;
import com.pointswell.ab.ab.*;
import com.pointswell.ab.ab.service.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class SysFxRates.
 * @see com.pointswell.ab.ab.SysFxRates
 */
@RestController(value = "Ab.SysFxRatesController")
@RequestMapping("/ab/SysFxRates")
@Api(description = "Exposes APIs to work with SysFxRates resource.", value = "SysFxRatesController")
public class SysFxRatesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SysFxRatesController.class);

    @Autowired
    @Qualifier("ab.SysFxRatesService")
    private SysFxRatesService sysFxRatesService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of SysFxRates instances matching the search criteria.")
    public Page<SysFxRates> findSysFxRatess(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering SysFxRatess list");
        return sysFxRatesService.findAll(queryFilters, pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of SysFxRates instances.")
    public Page<SysFxRates> getSysFxRatess(Pageable pageable) {
        LOGGER.debug("Rendering SysFxRatess list");
        return sysFxRatesService.findAll(pageable);
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the SysFxRates instance associated with the given id.")
    public SysFxRates getSysFxRates(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting SysFxRates with id: {}", id);
        SysFxRates instance = sysFxRatesService.findById(id);
        LOGGER.debug("SysFxRates details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes the SysFxRates instance associated with the given id.")
    public boolean deleteSysFxRates(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting SysFxRates with id: {}", id);
        SysFxRates deleted = sysFxRatesService.delete(id);
        return deleted != null;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates the SysFxRates instance associated with the given id.")
    public SysFxRates editSysFxRates(@PathVariable("id") Integer id, @RequestBody SysFxRates instance) throws EntityNotFoundException {
        LOGGER.debug("Editing SysFxRates with id: {}", instance.getSysFxRateid());
        instance.setSysFxRateid(id);
        instance = sysFxRatesService.update(instance);
        LOGGER.debug("SysFxRates details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ApiOperation(value = "Creates a new SysFxRates instance.")
    public SysFxRates createSysFxRates(@RequestBody SysFxRates instance) {
        LOGGER.debug("Create SysFxRates with information: {}", instance);
        instance = sysFxRatesService.create(instance);
        LOGGER.debug("Created SysFxRates with information: {}", instance);
        return instance;
    }

    /**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
    protected void setSysFxRatesService(SysFxRatesService service) {
        this.sysFxRatesService = service;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of SysFxRates instances.")
    public Long countAllSysFxRatess() {
        LOGGER.debug("counting SysFxRatess");
        Long count = sysFxRatesService.countAll();
        return count;
    }
}
