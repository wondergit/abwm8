/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import com.pointswell.ab.ab.service.CntLogService;
import com.pointswell.ab.ab.service.CntMethodService;
import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.TypeMismatchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.util.WMMultipartUtils;
import com.wavemaker.runtime.util.WMRuntimeUtils;
import com.wordnik.swagger.annotations.*;
import com.pointswell.ab.ab.*;
import com.pointswell.ab.ab.service.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class CntMethod.
 * @see com.pointswell.ab.ab.CntMethod
 */
@RestController(value = "Ab.CntMethodController")
@RequestMapping("/ab/CntMethod")
@Api(description = "Exposes APIs to work with CntMethod resource.", value = "CntMethodController")
public class CntMethodController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CntMethodController.class);

    @Autowired
    @Qualifier("ab.CntMethodService")
    private CntMethodService cntMethodService;

    @Autowired
    @Qualifier("ab.CntLogService")
    private CntLogService cntLogService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of CntMethod instances matching the search criteria.")
    public Page<CntMethod> findCntMethods(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering CntMethods list");
        return cntMethodService.findAll(queryFilters, pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of CntMethod instances.")
    public Page<CntMethod> getCntMethods(Pageable pageable) {
        LOGGER.debug("Rendering CntMethods list");
        return cntMethodService.findAll(pageable);
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the CntMethod instance associated with the given id.")
    public CntMethod getCntMethod(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting CntMethod with id: {}", id);
        CntMethod instance = cntMethodService.findById(id);
        LOGGER.debug("CntMethod details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes the CntMethod instance associated with the given id.")
    public boolean deleteCntMethod(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting CntMethod with id: {}", id);
        CntMethod deleted = cntMethodService.delete(id);
        return deleted != null;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates the CntMethod instance associated with the given id.")
    public CntMethod editCntMethod(@PathVariable("id") Integer id, @RequestBody CntMethod instance) throws EntityNotFoundException {
        LOGGER.debug("Editing CntMethod with id: {}", instance.getCntMethodid());
        instance.setCntMethodid(id);
        instance = cntMethodService.update(instance);
        LOGGER.debug("CntMethod details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}/cntLogs", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the cntLogs instance associated with the given id.")
    public Page<CntLog> findAssociatedcntLogs(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated cntLogs");
        return cntLogService.findAssociatedValues(id, "cntMethod", "cntMethodid", pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ApiOperation(value = "Creates a new CntMethod instance.")
    public CntMethod createCntMethod(@RequestBody CntMethod instance) {
        LOGGER.debug("Create CntMethod with information: {}", instance);
        instance = cntMethodService.create(instance);
        LOGGER.debug("Created CntMethod with information: {}", instance);
        return instance;
    }

    /**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
    protected void setCntMethodService(CntMethodService service) {
        this.cntMethodService = service;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of CntMethod instances.")
    public Long countAllCntMethods() {
        LOGGER.debug("counting CntMethods");
        Long count = cntMethodService.countAll();
        return count;
    }
}
