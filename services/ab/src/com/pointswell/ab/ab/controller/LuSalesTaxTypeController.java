/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import com.pointswell.ab.ab.service.CntSalestaxService;
import com.pointswell.ab.ab.service.LuSalesTaxTypeService;
import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.TypeMismatchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.util.WMMultipartUtils;
import com.wavemaker.runtime.util.WMRuntimeUtils;
import com.wordnik.swagger.annotations.*;
import com.pointswell.ab.ab.*;
import com.pointswell.ab.ab.service.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class LuSalesTaxType.
 * @see com.pointswell.ab.ab.LuSalesTaxType
 */
@RestController(value = "Ab.LuSalesTaxTypeController")
@RequestMapping("/ab/LuSalesTaxType")
@Api(description = "Exposes APIs to work with LuSalesTaxType resource.", value = "LuSalesTaxTypeController")
public class LuSalesTaxTypeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LuSalesTaxTypeController.class);

    @Autowired
    @Qualifier("ab.LuSalesTaxTypeService")
    private LuSalesTaxTypeService luSalesTaxTypeService;

    @Autowired
    @Qualifier("ab.CntSalestaxService")
    private CntSalestaxService cntSalestaxService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of LuSalesTaxType instances matching the search criteria.")
    public Page<LuSalesTaxType> findLuSalesTaxTypes(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering LuSalesTaxTypes list");
        return luSalesTaxTypeService.findAll(queryFilters, pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of LuSalesTaxType instances.")
    public Page<LuSalesTaxType> getLuSalesTaxTypes(Pageable pageable) {
        LOGGER.debug("Rendering LuSalesTaxTypes list");
        return luSalesTaxTypeService.findAll(pageable);
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the LuSalesTaxType instance associated with the given id.")
    public LuSalesTaxType getLuSalesTaxType(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting LuSalesTaxType with id: {}", id);
        LuSalesTaxType instance = luSalesTaxTypeService.findById(id);
        LOGGER.debug("LuSalesTaxType details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes the LuSalesTaxType instance associated with the given id.")
    public boolean deleteLuSalesTaxType(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting LuSalesTaxType with id: {}", id);
        LuSalesTaxType deleted = luSalesTaxTypeService.delete(id);
        return deleted != null;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates the LuSalesTaxType instance associated with the given id.")
    public LuSalesTaxType editLuSalesTaxType(@PathVariable("id") Integer id, @RequestBody LuSalesTaxType instance) throws EntityNotFoundException {
        LOGGER.debug("Editing LuSalesTaxType with id: {}", instance.getLuSalesTaxTypeid());
        instance.setLuSalesTaxTypeid(id);
        instance = luSalesTaxTypeService.update(instance);
        LOGGER.debug("LuSalesTaxType details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}/cntSalestaxes", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the cntSalestaxes instance associated with the given id.")
    public Page<CntSalestax> findAssociatedcntSalestaxes(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated cntSalestaxes");
        return cntSalestaxService.findAssociatedValues(id, "luSalesTaxType", "luSalesTaxTypeid", pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ApiOperation(value = "Creates a new LuSalesTaxType instance.")
    public LuSalesTaxType createLuSalesTaxType(@RequestBody LuSalesTaxType instance) {
        LOGGER.debug("Create LuSalesTaxType with information: {}", instance);
        instance = luSalesTaxTypeService.create(instance);
        LOGGER.debug("Created LuSalesTaxType with information: {}", instance);
        return instance;
    }

    /**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
    protected void setLuSalesTaxTypeService(LuSalesTaxTypeService service) {
        this.luSalesTaxTypeService = service;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of LuSalesTaxType instances.")
    public Long countAllLuSalesTaxTypes() {
        LOGGER.debug("counting LuSalesTaxTypes");
        Long count = luSalesTaxTypeService.countAll();
        return count;
    }
}
