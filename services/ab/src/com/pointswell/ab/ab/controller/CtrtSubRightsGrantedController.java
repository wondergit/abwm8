/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import com.pointswell.ab.ab.service.CtrtSubRightsGrantedService;
import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.TypeMismatchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.util.WMMultipartUtils;
import com.wavemaker.runtime.util.WMRuntimeUtils;
import com.wordnik.swagger.annotations.*;
import com.pointswell.ab.ab.*;
import com.pointswell.ab.ab.service.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class CtrtSubRightsGranted.
 * @see com.pointswell.ab.ab.CtrtSubRightsGranted
 */
@RestController(value = "Ab.CtrtSubRightsGrantedController")
@RequestMapping("/ab/CtrtSubRightsGranted")
@Api(description = "Exposes APIs to work with CtrtSubRightsGranted resource.", value = "CtrtSubRightsGrantedController")
public class CtrtSubRightsGrantedController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CtrtSubRightsGrantedController.class);

    @Autowired
    @Qualifier("ab.CtrtSubRightsGrantedService")
    private CtrtSubRightsGrantedService ctrtSubRightsGrantedService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of CtrtSubRightsGranted instances matching the search criteria.")
    public Page<CtrtSubRightsGranted> findCtrtSubRightsGranteds(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering CtrtSubRightsGranteds list");
        return ctrtSubRightsGrantedService.findAll(queryFilters, pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of CtrtSubRightsGranted instances.")
    public Page<CtrtSubRightsGranted> getCtrtSubRightsGranteds(Pageable pageable) {
        LOGGER.debug("Rendering CtrtSubRightsGranteds list");
        return ctrtSubRightsGrantedService.findAll(pageable);
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the CtrtSubRightsGranted instance associated with the given id.")
    public CtrtSubRightsGranted getCtrtSubRightsGranted(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting CtrtSubRightsGranted with id: {}", id);
        CtrtSubRightsGranted instance = ctrtSubRightsGrantedService.findById(id);
        LOGGER.debug("CtrtSubRightsGranted details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes the CtrtSubRightsGranted instance associated with the given id.")
    public boolean deleteCtrtSubRightsGranted(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting CtrtSubRightsGranted with id: {}", id);
        CtrtSubRightsGranted deleted = ctrtSubRightsGrantedService.delete(id);
        return deleted != null;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates the CtrtSubRightsGranted instance associated with the given id.")
    public CtrtSubRightsGranted editCtrtSubRightsGranted(@PathVariable("id") Integer id, @RequestBody CtrtSubRightsGranted instance) throws EntityNotFoundException {
        LOGGER.debug("Editing CtrtSubRightsGranted with id: {}", instance.getCtrtSubRightsGrantedid());
        instance.setCtrtSubRightsGrantedid(id);
        instance = ctrtSubRightsGrantedService.update(instance);
        LOGGER.debug("CtrtSubRightsGranted details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ApiOperation(value = "Creates a new CtrtSubRightsGranted instance.")
    public CtrtSubRightsGranted createCtrtSubRightsGranted(@RequestBody CtrtSubRightsGranted instance) {
        LOGGER.debug("Create CtrtSubRightsGranted with information: {}", instance);
        instance = ctrtSubRightsGrantedService.create(instance);
        LOGGER.debug("Created CtrtSubRightsGranted with information: {}", instance);
        return instance;
    }

    /**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
    protected void setCtrtSubRightsGrantedService(CtrtSubRightsGrantedService service) {
        this.ctrtSubRightsGrantedService = service;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of CtrtSubRightsGranted instances.")
    public Long countAllCtrtSubRightsGranteds() {
        LOGGER.debug("counting CtrtSubRightsGranteds");
        Long count = ctrtSubRightsGrantedService.countAll();
        return count;
    }
}
