/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import com.pointswell.ab.ab.service.LuGenreService;
import com.pointswell.ab.ab.service.LuGenreSubService;
import com.pointswell.ab.ab.service.LuGenreTransService;
import com.pointswell.ab.ab.service.WkHasGenreService;
import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.TypeMismatchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.util.WMMultipartUtils;
import com.wavemaker.runtime.util.WMRuntimeUtils;
import com.wordnik.swagger.annotations.*;
import com.pointswell.ab.ab.*;
import com.pointswell.ab.ab.service.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class LuGenre.
 * @see com.pointswell.ab.ab.LuGenre
 */
@RestController(value = "Ab.LuGenreController")
@RequestMapping("/ab/LuGenre")
@Api(description = "Exposes APIs to work with LuGenre resource.", value = "LuGenreController")
public class LuGenreController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LuGenreController.class);

    @Autowired
    @Qualifier("ab.LuGenreService")
    private LuGenreService luGenreService;

    @Autowired
    @Qualifier("ab.LuGenreTransService")
    private LuGenreTransService luGenreTransService;

    @Autowired
    @Qualifier("ab.WkHasGenreService")
    private WkHasGenreService wkHasGenreService;

    @Autowired
    @Qualifier("ab.LuGenreSubService")
    private LuGenreSubService luGenreSubService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of LuGenre instances matching the search criteria.")
    public Page<LuGenre> findLuGenres(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering LuGenres list");
        return luGenreService.findAll(queryFilters, pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of LuGenre instances.")
    public Page<LuGenre> getLuGenres(Pageable pageable) {
        LOGGER.debug("Rendering LuGenres list");
        return luGenreService.findAll(pageable);
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the LuGenre instance associated with the given id.")
    public LuGenre getLuGenre(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting LuGenre with id: {}", id);
        LuGenre instance = luGenreService.findById(id);
        LOGGER.debug("LuGenre details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes the LuGenre instance associated with the given id.")
    public boolean deleteLuGenre(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting LuGenre with id: {}", id);
        LuGenre deleted = luGenreService.delete(id);
        return deleted != null;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates the LuGenre instance associated with the given id.")
    public LuGenre editLuGenre(@PathVariable("id") Integer id, @RequestBody LuGenre instance) throws EntityNotFoundException {
        LOGGER.debug("Editing LuGenre with id: {}", instance.getLuGenreid());
        instance.setLuGenreid(id);
        instance = luGenreService.update(instance);
        LOGGER.debug("LuGenre details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}/wkHasGenres", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the wkHasGenres instance associated with the given id.")
    public Page<WkHasGenre> findAssociatedwkHasGenres(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated wkHasGenres");
        return wkHasGenreService.findAssociatedValues(id, "luGenre", "luGenreid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/luGenreTranses", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the luGenreTranses instance associated with the given id.")
    public Page<LuGenreTrans> findAssociatedluGenreTranses(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated luGenreTranses");
        return luGenreTransService.findAssociatedValues(id, "luGenre", "luGenreid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/luGenreSubs", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the luGenreSubs instance associated with the given id.")
    public Page<LuGenreSub> findAssociatedluGenreSubs(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated luGenreSubs");
        return luGenreSubService.findAssociatedValues(id, "luGenre", "luGenreid", pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ApiOperation(value = "Creates a new LuGenre instance.")
    public LuGenre createLuGenre(@RequestBody LuGenre instance) {
        LOGGER.debug("Create LuGenre with information: {}", instance);
        instance = luGenreService.create(instance);
        LOGGER.debug("Created LuGenre with information: {}", instance);
        return instance;
    }

    /**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
    protected void setLuGenreService(LuGenreService service) {
        this.luGenreService = service;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of LuGenre instances.")
    public Long countAllLuGenres() {
        LOGGER.debug("counting LuGenres");
        Long count = luGenreService.countAll();
        return count;
    }
}
