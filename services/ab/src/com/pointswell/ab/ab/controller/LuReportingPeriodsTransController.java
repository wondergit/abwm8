/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import com.pointswell.ab.ab.service.LuReportingPeriodsTransService;
import org.springframework.web.bind.annotation.RequestBody;
import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.TypeMismatchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.util.WMMultipartUtils;
import com.wavemaker.runtime.util.WMRuntimeUtils;
import com.wordnik.swagger.annotations.*;
import com.pointswell.ab.ab.*;
import com.pointswell.ab.ab.service.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class LuReportingPeriodsTrans.
 * @see com.pointswell.ab.ab.LuReportingPeriodsTrans
 */
@RestController(value = "Ab.LuReportingPeriodsTransController")
@RequestMapping("/ab/LuReportingPeriodsTrans")
@Api(description = "Exposes APIs to work with LuReportingPeriodsTrans resource.", value = "LuReportingPeriodsTransController")
public class LuReportingPeriodsTransController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LuReportingPeriodsTransController.class);

    @Autowired
    @Qualifier("ab.LuReportingPeriodsTransService")
    private LuReportingPeriodsTransService luReportingPeriodsTransService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of LuReportingPeriodsTrans instances matching the search criteria.")
    public Page<LuReportingPeriodsTrans> findLuReportingPeriodsTranss(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering LuReportingPeriodsTranss list");
        return luReportingPeriodsTransService.findAll(queryFilters, pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of LuReportingPeriodsTrans instances.")
    public Page<LuReportingPeriodsTrans> getLuReportingPeriodsTranss(Pageable pageable) {
        LOGGER.debug("Rendering LuReportingPeriodsTranss list");
        return luReportingPeriodsTransService.findAll(pageable);
    }

    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the LuReportingPeriodsTrans instance associated with the given composite-id.")
    public LuReportingPeriodsTrans getLuReportingPeriodsTrans(@RequestParam("luReportingPeriodsLuReportingPeriodsid") Integer luReportingPeriodsLuReportingPeriodsid, @RequestParam("luLanguageLuLanguageid") Integer luLanguageLuLanguageid) throws EntityNotFoundException {
        LuReportingPeriodsTransId temp = new LuReportingPeriodsTransId();
        temp.setLuReportingPeriodsLuReportingPeriodsid(luReportingPeriodsLuReportingPeriodsid);
        temp.setLuLanguageLuLanguageid(luLanguageLuLanguageid);
        LOGGER.debug("Getting LuReportingPeriodsTrans with id: {}", temp);
        LuReportingPeriodsTrans instance = luReportingPeriodsTransService.findById(temp);
        LOGGER.debug("LuReportingPeriodsTrans details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes the LuReportingPeriodsTrans instance associated with the given composite-id.")
    public boolean deleteLuReportingPeriodsTrans(@RequestParam("luReportingPeriodsLuReportingPeriodsid") Integer luReportingPeriodsLuReportingPeriodsid, @RequestParam("luLanguageLuLanguageid") Integer luLanguageLuLanguageid) throws EntityNotFoundException {
        LuReportingPeriodsTransId temp = new LuReportingPeriodsTransId();
        temp.setLuReportingPeriodsLuReportingPeriodsid(luReportingPeriodsLuReportingPeriodsid);
        temp.setLuLanguageLuLanguageid(luLanguageLuLanguageid);
        LOGGER.debug("Deleting LuReportingPeriodsTrans with id: {}", temp);
        LuReportingPeriodsTrans deleted = luReportingPeriodsTransService.delete(temp);
        return deleted != null;
    }

    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates the LuReportingPeriodsTrans instance associated with the given composite-id.")
    public LuReportingPeriodsTrans editLuReportingPeriodsTrans(@RequestParam("luReportingPeriodsLuReportingPeriodsid") Integer luReportingPeriodsLuReportingPeriodsid, @RequestParam("luLanguageLuLanguageid") Integer luLanguageLuLanguageid, @RequestBody LuReportingPeriodsTrans instance) throws EntityNotFoundException {
        LuReportingPeriodsTransId temp = new LuReportingPeriodsTransId();
        temp.setLuReportingPeriodsLuReportingPeriodsid(luReportingPeriodsLuReportingPeriodsid);
        temp.setLuLanguageLuLanguageid(luLanguageLuLanguageid);
        luReportingPeriodsTransService.delete(temp);
        instance = luReportingPeriodsTransService.create(instance);
        LOGGER.debug("LuReportingPeriodsTrans details with id is updated: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ApiOperation(value = "Creates a new LuReportingPeriodsTrans instance.")
    public LuReportingPeriodsTrans createLuReportingPeriodsTrans(@RequestBody LuReportingPeriodsTrans instance) {
        LOGGER.debug("Create LuReportingPeriodsTrans with information: {}", instance);
        instance = luReportingPeriodsTransService.create(instance);
        LOGGER.debug("Created LuReportingPeriodsTrans with information: {}", instance);
        return instance;
    }

    /**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
    protected void setLuReportingPeriodsTransService(LuReportingPeriodsTransService service) {
        this.luReportingPeriodsTransService = service;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of LuReportingPeriodsTrans instances.")
    public Long countAllLuReportingPeriodsTranss() {
        LOGGER.debug("counting LuReportingPeriodsTranss");
        Long count = luReportingPeriodsTransService.countAll();
        return count;
    }
}
