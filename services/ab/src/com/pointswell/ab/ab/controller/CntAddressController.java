/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import com.pointswell.ab.ab.service.CntAddressService;
import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.TypeMismatchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.util.WMMultipartUtils;
import com.wavemaker.runtime.util.WMRuntimeUtils;
import com.wordnik.swagger.annotations.*;
import com.pointswell.ab.ab.*;
import com.pointswell.ab.ab.service.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class CntAddress.
 * @see com.pointswell.ab.ab.CntAddress
 */
@RestController(value = "Ab.CntAddressController")
@RequestMapping("/ab/CntAddress")
@Api(description = "Exposes APIs to work with CntAddress resource.", value = "CntAddressController")
public class CntAddressController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CntAddressController.class);

    @Autowired
    @Qualifier("ab.CntAddressService")
    private CntAddressService cntAddressService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of CntAddress instances matching the search criteria.")
    public Page<CntAddress> findCntAddresss(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering CntAddresss list");
        return cntAddressService.findAll(queryFilters, pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of CntAddress instances.")
    public Page<CntAddress> getCntAddresss(Pageable pageable) {
        LOGGER.debug("Rendering CntAddresss list");
        return cntAddressService.findAll(pageable);
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the CntAddress instance associated with the given id.")
    public CntAddress getCntAddress(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting CntAddress with id: {}", id);
        CntAddress instance = cntAddressService.findById(id);
        LOGGER.debug("CntAddress details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes the CntAddress instance associated with the given id.")
    public boolean deleteCntAddress(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting CntAddress with id: {}", id);
        CntAddress deleted = cntAddressService.delete(id);
        return deleted != null;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates the CntAddress instance associated with the given id.")
    public CntAddress editCntAddress(@PathVariable("id") Integer id, @RequestBody CntAddress instance) throws EntityNotFoundException {
        LOGGER.debug("Editing CntAddress with id: {}", instance.getAddressid());
        instance.setAddressid(id);
        instance = cntAddressService.update(instance);
        LOGGER.debug("CntAddress details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ApiOperation(value = "Creates a new CntAddress instance.")
    public CntAddress createCntAddress(@RequestBody CntAddress instance) {
        LOGGER.debug("Create CntAddress with information: {}", instance);
        instance = cntAddressService.create(instance);
        LOGGER.debug("Created CntAddress with information: {}", instance);
        return instance;
    }

    /**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
    protected void setCntAddressService(CntAddressService service) {
        this.cntAddressService = service;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of CntAddress instances.")
    public Long countAllCntAddresss() {
        LOGGER.debug("counting CntAddresss");
        Long count = cntAddressService.countAll();
        return count;
    }
}
