/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import com.pointswell.ab.ab.service.LuGlCodesService;
import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.TypeMismatchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.util.WMMultipartUtils;
import com.wavemaker.runtime.util.WMRuntimeUtils;
import com.wordnik.swagger.annotations.*;
import com.pointswell.ab.ab.*;
import com.pointswell.ab.ab.service.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class LuGlCodes.
 * @see com.pointswell.ab.ab.LuGlCodes
 */
@RestController(value = "Ab.LuGlCodesController")
@RequestMapping("/ab/LuGlCodes")
@Api(description = "Exposes APIs to work with LuGlCodes resource.", value = "LuGlCodesController")
public class LuGlCodesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LuGlCodesController.class);

    @Autowired
    @Qualifier("ab.LuGlCodesService")
    private LuGlCodesService luGlCodesService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of LuGlCodes instances matching the search criteria.")
    public Page<LuGlCodes> findLuGlCodess(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering LuGlCodess list");
        return luGlCodesService.findAll(queryFilters, pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of LuGlCodes instances.")
    public Page<LuGlCodes> getLuGlCodess(Pageable pageable) {
        LOGGER.debug("Rendering LuGlCodess list");
        return luGlCodesService.findAll(pageable);
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the LuGlCodes instance associated with the given id.")
    public LuGlCodes getLuGlCodes(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting LuGlCodes with id: {}", id);
        LuGlCodes instance = luGlCodesService.findById(id);
        LOGGER.debug("LuGlCodes details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes the LuGlCodes instance associated with the given id.")
    public boolean deleteLuGlCodes(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting LuGlCodes with id: {}", id);
        LuGlCodes deleted = luGlCodesService.delete(id);
        return deleted != null;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates the LuGlCodes instance associated with the given id.")
    public LuGlCodes editLuGlCodes(@PathVariable("id") Integer id, @RequestBody LuGlCodes instance) throws EntityNotFoundException {
        LOGGER.debug("Editing LuGlCodes with id: {}", instance.getLuGlCodesid());
        instance.setLuGlCodesid(id);
        instance = luGlCodesService.update(instance);
        LOGGER.debug("LuGlCodes details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ApiOperation(value = "Creates a new LuGlCodes instance.")
    public LuGlCodes createLuGlCodes(@RequestBody LuGlCodes instance) {
        LOGGER.debug("Create LuGlCodes with information: {}", instance);
        instance = luGlCodesService.create(instance);
        LOGGER.debug("Created LuGlCodes with information: {}", instance);
        return instance;
    }

    /**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
    protected void setLuGlCodesService(LuGlCodesService service) {
        this.luGlCodesService = service;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of LuGlCodes instances.")
    public Long countAllLuGlCodess() {
        LOGGER.debug("counting LuGlCodess");
        Long count = luGlCodesService.countAll();
        return count;
    }
}
