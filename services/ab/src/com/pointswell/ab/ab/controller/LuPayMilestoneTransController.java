/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import com.pointswell.ab.ab.service.LuPayMilestoneTransService;
import org.springframework.web.bind.annotation.RequestBody;
import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.TypeMismatchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.util.WMMultipartUtils;
import com.wavemaker.runtime.util.WMRuntimeUtils;
import com.wordnik.swagger.annotations.*;
import com.pointswell.ab.ab.*;
import com.pointswell.ab.ab.service.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class LuPayMilestoneTrans.
 * @see com.pointswell.ab.ab.LuPayMilestoneTrans
 */
@RestController(value = "Ab.LuPayMilestoneTransController")
@RequestMapping("/ab/LuPayMilestoneTrans")
@Api(description = "Exposes APIs to work with LuPayMilestoneTrans resource.", value = "LuPayMilestoneTransController")
public class LuPayMilestoneTransController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LuPayMilestoneTransController.class);

    @Autowired
    @Qualifier("ab.LuPayMilestoneTransService")
    private LuPayMilestoneTransService luPayMilestoneTransService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of LuPayMilestoneTrans instances matching the search criteria.")
    public Page<LuPayMilestoneTrans> findLuPayMilestoneTranss(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering LuPayMilestoneTranss list");
        return luPayMilestoneTransService.findAll(queryFilters, pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of LuPayMilestoneTrans instances.")
    public Page<LuPayMilestoneTrans> getLuPayMilestoneTranss(Pageable pageable) {
        LOGGER.debug("Rendering LuPayMilestoneTranss list");
        return luPayMilestoneTransService.findAll(pageable);
    }

    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the LuPayMilestoneTrans instance associated with the given composite-id.")
    public LuPayMilestoneTrans getLuPayMilestoneTrans(@RequestParam("luLanguageid") Integer luLanguageid, @RequestParam("luPayMilestonesid") Integer luPayMilestonesid) throws EntityNotFoundException {
        LuPayMilestoneTransId temp = new LuPayMilestoneTransId();
        temp.setLuLanguageid(luLanguageid);
        temp.setLuPayMilestonesid(luPayMilestonesid);
        LOGGER.debug("Getting LuPayMilestoneTrans with id: {}", temp);
        LuPayMilestoneTrans instance = luPayMilestoneTransService.findById(temp);
        LOGGER.debug("LuPayMilestoneTrans details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes the LuPayMilestoneTrans instance associated with the given composite-id.")
    public boolean deleteLuPayMilestoneTrans(@RequestParam("luLanguageid") Integer luLanguageid, @RequestParam("luPayMilestonesid") Integer luPayMilestonesid) throws EntityNotFoundException {
        LuPayMilestoneTransId temp = new LuPayMilestoneTransId();
        temp.setLuLanguageid(luLanguageid);
        temp.setLuPayMilestonesid(luPayMilestonesid);
        LOGGER.debug("Deleting LuPayMilestoneTrans with id: {}", temp);
        LuPayMilestoneTrans deleted = luPayMilestoneTransService.delete(temp);
        return deleted != null;
    }

    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates the LuPayMilestoneTrans instance associated with the given composite-id.")
    public LuPayMilestoneTrans editLuPayMilestoneTrans(@RequestParam("luLanguageid") Integer luLanguageid, @RequestParam("luPayMilestonesid") Integer luPayMilestonesid, @RequestBody LuPayMilestoneTrans instance) throws EntityNotFoundException {
        LuPayMilestoneTransId temp = new LuPayMilestoneTransId();
        temp.setLuLanguageid(luLanguageid);
        temp.setLuPayMilestonesid(luPayMilestonesid);
        luPayMilestoneTransService.delete(temp);
        instance = luPayMilestoneTransService.create(instance);
        LOGGER.debug("LuPayMilestoneTrans details with id is updated: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ApiOperation(value = "Creates a new LuPayMilestoneTrans instance.")
    public LuPayMilestoneTrans createLuPayMilestoneTrans(@RequestBody LuPayMilestoneTrans instance) {
        LOGGER.debug("Create LuPayMilestoneTrans with information: {}", instance);
        instance = luPayMilestoneTransService.create(instance);
        LOGGER.debug("Created LuPayMilestoneTrans with information: {}", instance);
        return instance;
    }

    /**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
    protected void setLuPayMilestoneTransService(LuPayMilestoneTransService service) {
        this.luPayMilestoneTransService = service;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of LuPayMilestoneTrans instances.")
    public Long countAllLuPayMilestoneTranss() {
        LOGGER.debug("counting LuPayMilestoneTranss");
        Long count = luPayMilestoneTransService.countAll();
        return count;
    }
}
