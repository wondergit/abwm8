/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import com.pointswell.ab.ab.service.CtrtPayMilestoneService;
import com.pointswell.ab.ab.service.LuPayMilestoneService;
import com.pointswell.ab.ab.service.LuPayMilestoneTransService;
import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.TypeMismatchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.util.WMMultipartUtils;
import com.wavemaker.runtime.util.WMRuntimeUtils;
import com.wordnik.swagger.annotations.*;
import com.pointswell.ab.ab.*;
import com.pointswell.ab.ab.service.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class LuPayMilestone.
 * @see com.pointswell.ab.ab.LuPayMilestone
 */
@RestController(value = "Ab.LuPayMilestoneController")
@RequestMapping("/ab/LuPayMilestone")
@Api(description = "Exposes APIs to work with LuPayMilestone resource.", value = "LuPayMilestoneController")
public class LuPayMilestoneController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LuPayMilestoneController.class);

    @Autowired
    @Qualifier("ab.LuPayMilestoneService")
    private LuPayMilestoneService luPayMilestoneService;

    @Autowired
    @Qualifier("ab.CtrtPayMilestoneService")
    private CtrtPayMilestoneService ctrtPayMilestoneService;

    @Autowired
    @Qualifier("ab.LuPayMilestoneTransService")
    private LuPayMilestoneTransService luPayMilestoneTransService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of LuPayMilestone instances matching the search criteria.")
    public Page<LuPayMilestone> findLuPayMilestones(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering LuPayMilestones list");
        return luPayMilestoneService.findAll(queryFilters, pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of LuPayMilestone instances.")
    public Page<LuPayMilestone> getLuPayMilestones(Pageable pageable) {
        LOGGER.debug("Rendering LuPayMilestones list");
        return luPayMilestoneService.findAll(pageable);
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the LuPayMilestone instance associated with the given id.")
    public LuPayMilestone getLuPayMilestone(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting LuPayMilestone with id: {}", id);
        LuPayMilestone instance = luPayMilestoneService.findById(id);
        LOGGER.debug("LuPayMilestone details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes the LuPayMilestone instance associated with the given id.")
    public boolean deleteLuPayMilestone(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting LuPayMilestone with id: {}", id);
        LuPayMilestone deleted = luPayMilestoneService.delete(id);
        return deleted != null;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates the LuPayMilestone instance associated with the given id.")
    public LuPayMilestone editLuPayMilestone(@PathVariable("id") Integer id, @RequestBody LuPayMilestone instance) throws EntityNotFoundException {
        LOGGER.debug("Editing LuPayMilestone with id: {}", instance.getLuPayMilestonesid());
        instance.setLuPayMilestonesid(id);
        instance = luPayMilestoneService.update(instance);
        LOGGER.debug("LuPayMilestone details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}/luPayMilestoneTranses", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the luPayMilestoneTranses instance associated with the given id.")
    public Page<LuPayMilestoneTrans> findAssociatedluPayMilestoneTranses(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated luPayMilestoneTranses");
        return luPayMilestoneTransService.findAssociatedValues(id, "luPayMilestone", "luPayMilestonesid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/ctrtPayMilestones", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the ctrtPayMilestones instance associated with the given id.")
    public Page<CtrtPayMilestone> findAssociatedctrtPayMilestones(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated ctrtPayMilestones");
        return ctrtPayMilestoneService.findAssociatedValues(id, "luPayMilestone", "luPayMilestonesid", pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ApiOperation(value = "Creates a new LuPayMilestone instance.")
    public LuPayMilestone createLuPayMilestone(@RequestBody LuPayMilestone instance) {
        LOGGER.debug("Create LuPayMilestone with information: {}", instance);
        instance = luPayMilestoneService.create(instance);
        LOGGER.debug("Created LuPayMilestone with information: {}", instance);
        return instance;
    }

    /**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
    protected void setLuPayMilestoneService(LuPayMilestoneService service) {
        this.luPayMilestoneService = service;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of LuPayMilestone instances.")
    public Long countAllLuPayMilestones() {
        LOGGER.debug("counting LuPayMilestones");
        Long count = luPayMilestoneService.countAll();
        return count;
    }
}
