/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import com.pointswell.ab.ab.service.CntAddressService;
import com.pointswell.ab.ab.service.CntBankdetailsService;
import com.pointswell.ab.ab.service.CntDefaultcommService;
import com.pointswell.ab.ab.service.CntLogService;
import com.pointswell.ab.ab.service.CntMethodService;
import com.pointswell.ab.ab.service.CntPaysplitsService;
import com.pointswell.ab.ab.service.CntPoaService;
import com.pointswell.ab.ab.service.CntSalestaxService;
import com.pointswell.ab.ab.service.CntSupportService;
import com.pointswell.ab.ab.service.CntTaxexemptionService;
import com.pointswell.ab.ab.service.ContactService;
import com.pointswell.ab.ab.service.ContractService;
import com.pointswell.ab.ab.service.CtrtCommShareService;
import com.pointswell.ab.ab.service.CtrtThirdPartyPayeesService;
import com.pointswell.ab.ab.service.PmtCashbookService;
import com.pointswell.ab.ab.service.PmtContactAccService;
import com.pointswell.ab.ab.service.PmtDeductionsService;
import com.pointswell.ab.ab.service.PmtFxGainlossService;
import com.pointswell.ab.ab.service.PmtNominalLedgerService;
import com.pointswell.ab.ab.service.PmtSalesTaxService;
import com.pointswell.ab.ab.service.WkHasContributorService;
import com.pointswell.ab.ab.service.WkHasProprietorsService;
import com.pointswell.ab.ab.service.WkSubmissionService;
import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.TypeMismatchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.util.WMMultipartUtils;
import com.wavemaker.runtime.util.WMRuntimeUtils;
import com.wordnik.swagger.annotations.*;
import com.pointswell.ab.ab.*;
import com.pointswell.ab.ab.service.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class Contact.
 * @see com.pointswell.ab.ab.Contact
 */
@RestController(value = "Ab.ContactController")
@RequestMapping("/ab/Contact")
@Api(description = "Exposes APIs to work with Contact resource.", value = "ContactController")
public class ContactController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContactController.class);

    @Autowired
    @Qualifier("ab.ContactService")
    private ContactService contactService;

    @Autowired
    @Qualifier("ab.PmtDeductionsService")
    private PmtDeductionsService pmtDeductionsService;

    @Autowired
    @Qualifier("ab.CntPoaService")
    private CntPoaService cntPoaService;

    @Autowired
    @Qualifier("ab.WkSubmissionService")
    private WkSubmissionService wkSubmissionService;

    @Autowired
    @Qualifier("ab.CntAddressService")
    private CntAddressService cntAddressService;

    @Autowired
    @Qualifier("ab.PmtFxGainlossService")
    private PmtFxGainlossService pmtFxGainlossService;

    @Autowired
    @Qualifier("ab.WkHasContributorService")
    private WkHasContributorService wkHasContributorService;

    @Autowired
    @Qualifier("ab.CntTaxexemptionService")
    private CntTaxexemptionService cntTaxexemptionService;

    @Autowired
    @Qualifier("ab.PmtContactAccService")
    private PmtContactAccService pmtContactAccService;

    @Autowired
    @Qualifier("ab.CntDefaultcommService")
    private CntDefaultcommService cntDefaultcommService;

    @Autowired
    @Qualifier("ab.WkHasProprietorsService")
    private WkHasProprietorsService wkHasProprietorsService;

    @Autowired
    @Qualifier("ab.ContractService")
    private ContractService contractService;

    @Autowired
    @Qualifier("ab.PmtNominalLedgerService")
    private PmtNominalLedgerService pmtNominalLedgerService;

    @Autowired
    @Qualifier("ab.PmtCashbookService")
    private PmtCashbookService pmtCashbookService;

    @Autowired
    @Qualifier("ab.CntPaysplitsService")
    private CntPaysplitsService cntPaysplitsService;

    @Autowired
    @Qualifier("ab.PmtSalesTaxService")
    private PmtSalesTaxService pmtSalesTaxService;

    @Autowired
    @Qualifier("ab.CtrtCommShareService")
    private CtrtCommShareService ctrtCommShareService;

    @Autowired
    @Qualifier("ab.CntBankdetailsService")
    private CntBankdetailsService cntBankdetailsService;

    @Autowired
    @Qualifier("ab.CntLogService")
    private CntLogService cntLogService;

    @Autowired
    @Qualifier("ab.CntMethodService")
    private CntMethodService cntMethodService;

    @Autowired
    @Qualifier("ab.CntSalestaxService")
    private CntSalestaxService cntSalestaxService;

    @Autowired
    @Qualifier("ab.CtrtThirdPartyPayeesService")
    private CtrtThirdPartyPayeesService ctrtThirdPartyPayeesService;

    @Autowired
    @Qualifier("ab.CntSupportService")
    private CntSupportService cntSupportService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of Contact instances matching the search criteria.")
    public Page<Contact> findContacts(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Contacts list");
        return contactService.findAll(queryFilters, pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of Contact instances.")
    public Page<Contact> getContacts(Pageable pageable) {
        LOGGER.debug("Rendering Contacts list");
        return contactService.findAll(pageable);
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the Contact instance associated with the given id.")
    public Contact getContact(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting Contact with id: {}", id);
        Contact instance = contactService.findById(id);
        LOGGER.debug("Contact details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes the Contact instance associated with the given id.")
    public boolean deleteContact(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting Contact with id: {}", id);
        Contact deleted = contactService.delete(id);
        return deleted != null;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates the Contact instance associated with the given id.")
    public Contact editContact(@PathVariable("id") Integer id, @RequestBody Contact instance) throws EntityNotFoundException {
        LOGGER.debug("Editing Contact with id: {}", instance.getContactid());
        instance.setContactid(id);
        instance = contactService.update(instance);
        LOGGER.debug("Contact details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}/cntPoasForContactid", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the cntPoasForContactid instance associated with the given id.")
    public Page<CntPoa> findAssociatedcntPoasForContactid(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated cntPoasForContactid");
        return cntPoaService.findAssociatedValues(id, "contactByAgent", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/contactsForParentcompany", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the contactsForParentcompany instance associated with the given id.")
    public Page<Contact> findAssociatedcontactsForParentcompany(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated contactsForParentcompany");
        return contactService.findAssociatedValues(id, "contactByParentcompany", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/contractsForBuyerContactid", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the contractsForBuyerContactid instance associated with the given id.")
    public Page<Contract> findAssociatedcontractsForBuyerContactid(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated contractsForBuyerContactid");
        return contractService.findAssociatedValues(id, "contactByBuyerContactid", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/pmtFxGainlossesForPmtFxGlRemitter", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the pmtFxGainlossesForPmtFxGlRemitter instance associated with the given id.")
    public Page<PmtFxGainloss> findAssociatedpmtFxGainlossesForPmtFxGlRemitter(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated pmtFxGainlossesForPmtFxGlRemitter");
        return pmtFxGainlossService.findAssociatedValues(id, "contactByPmtFxGlRemitter", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/cntMethods", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the cntMethods instance associated with the given id.")
    public Page<CntMethod> findAssociatedcntMethods(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated cntMethods");
        return cntMethodService.findAssociatedValues(id, "contact", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/pmtCashbooks", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the pmtCashbooks instance associated with the given id.")
    public Page<PmtCashbook> findAssociatedpmtCashbooks(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated pmtCashbooks");
        return pmtCashbookService.findAssociatedValues(id, "contact", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/pmtContactAccs", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the pmtContactAccs instance associated with the given id.")
    public Page<PmtContactAcc> findAssociatedpmtContactAccs(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated pmtContactAccs");
        return pmtContactAccService.findAssociatedValues(id, "contact", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/cntSupportsForContactidSeller", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the cntSupportsForContactidSeller instance associated with the given id.")
    public Page<CntSupport> findAssociatedcntSupportsForContactidSeller(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated cntSupportsForContactidSeller");
        return cntSupportService.findAssociatedValues(id, "contactByContactidSupport", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/cntBankdetailses", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the cntBankdetailses instance associated with the given id.")
    public Page<CntBankdetails> findAssociatedcntBankdetailses(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated cntBankdetailses");
        return cntBankdetailsService.findAssociatedValues(id, "contact", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/cntLogs", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the cntLogs instance associated with the given id.")
    public Page<CntLog> findAssociatedcntLogs(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated cntLogs");
        return cntLogService.findAssociatedValues(id, "contact", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/cntPaysplitses", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the cntPaysplitses instance associated with the given id.")
    public Page<CntPaysplits> findAssociatedcntPaysplitses(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated cntPaysplitses");
        return cntPaysplitsService.findAssociatedValues(id, "contact", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/contractsForSellingAgentContactid", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the contractsForSellingAgentContactid instance associated with the given id.")
    public Page<Contract> findAssociatedcontractsForSellingAgentContactid(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated contractsForSellingAgentContactid");
        return contractService.findAssociatedValues(id, "contactByBuyerContactid", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/ctrtThirdPartyPayeeses", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the ctrtThirdPartyPayeeses instance associated with the given id.")
    public Page<CtrtThirdPartyPayees> findAssociatedctrtThirdPartyPayeeses(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated ctrtThirdPartyPayeeses");
        return ctrtThirdPartyPayeesService.findAssociatedValues(id, "contact", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/contactsForIspseudonymof", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the contactsForIspseudonymof instance associated with the given id.")
    public Page<Contact> findAssociatedcontactsForIspseudonymof(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated contactsForIspseudonymof");
        return contactService.findAssociatedValues(id, "contactByParentcompany", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/pmtSalesTaxes", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the pmtSalesTaxes instance associated with the given id.")
    public Page<PmtSalesTax> findAssociatedpmtSalesTaxes(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated pmtSalesTaxes");
        return pmtSalesTaxService.findAssociatedValues(id, "contact", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/contactsForRelatedProp", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the contactsForRelatedProp instance associated with the given id.")
    public Page<Contact> findAssociatedcontactsForRelatedProp(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated contactsForRelatedProp");
        return contactService.findAssociatedValues(id, "contactByParentcompany", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/cntSalestaxes", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the cntSalestaxes instance associated with the given id.")
    public Page<CntSalestax> findAssociatedcntSalestaxes(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated cntSalestaxes");
        return cntSalestaxService.findAssociatedValues(id, "contact", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/pmtDeductionsesForContactidChargedby", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the pmtDeductionsesForContactidChargedby instance associated with the given id.")
    public Page<PmtDeductions> findAssociatedpmtDeductionsesForContactidChargedby(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated pmtDeductionsesForContactidChargedby");
        return pmtDeductionsService.findAssociatedValues(id, "contactByContactidChargedto", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/wkHasContributors", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the wkHasContributors instance associated with the given id.")
    public Page<WkHasContributor> findAssociatedwkHasContributors(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated wkHasContributors");
        return wkHasContributorService.findAssociatedValues(id, "contact", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/ctrtCommShares", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the ctrtCommShares instance associated with the given id.")
    public Page<CtrtCommShare> findAssociatedctrtCommShares(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated ctrtCommShares");
        return ctrtCommShareService.findAssociatedValues(id, "contact", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/cntSupportsForContactidSupport", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the cntSupportsForContactidSupport instance associated with the given id.")
    public Page<CntSupport> findAssociatedcntSupportsForContactidSupport(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated cntSupportsForContactidSupport");
        return cntSupportService.findAssociatedValues(id, "contactByContactidSupport", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/pmtDeductionsesForContactidChargedto", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the pmtDeductionsesForContactidChargedto instance associated with the given id.")
    public Page<PmtDeductions> findAssociatedpmtDeductionsesForContactidChargedto(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated pmtDeductionsesForContactidChargedto");
        return pmtDeductionsService.findAssociatedValues(id, "contactByContactidChargedto", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/cntDefaultcomms", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the cntDefaultcomms instance associated with the given id.")
    public Page<CntDefaultcomm> findAssociatedcntDefaultcomms(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated cntDefaultcomms");
        return cntDefaultcommService.findAssociatedValues(id, "contact", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/wkSubmissionsForSubmittedCo", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the wkSubmissionsForSubmittedCo instance associated with the given id.")
    public Page<WkSubmission> findAssociatedwkSubmissionsForSubmittedCo(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated wkSubmissionsForSubmittedCo");
        return wkSubmissionService.findAssociatedValues(id, "contactBySubmittedCo", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/pmtFxGainlossesForPmtFxGlReceiver", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the pmtFxGainlossesForPmtFxGlReceiver instance associated with the given id.")
    public Page<PmtFxGainloss> findAssociatedpmtFxGainlossesForPmtFxGlReceiver(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated pmtFxGainlossesForPmtFxGlReceiver");
        return pmtFxGainlossService.findAssociatedValues(id, "contactByPmtFxGlRemitter", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/cntPoasForAgent", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the cntPoasForAgent instance associated with the given id.")
    public Page<CntPoa> findAssociatedcntPoasForAgent(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated cntPoasForAgent");
        return cntPoaService.findAssociatedValues(id, "contactByAgent", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/contactsForIsemployedby", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the contactsForIsemployedby instance associated with the given id.")
    public Page<Contact> findAssociatedcontactsForIsemployedby(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated contactsForIsemployedby");
        return contactService.findAssociatedValues(id, "contactByParentcompany", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/pmtNominalLedgers", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the pmtNominalLedgers instance associated with the given id.")
    public Page<PmtNominalLedger> findAssociatedpmtNominalLedgers(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated pmtNominalLedgers");
        return pmtNominalLedgerService.findAssociatedValues(id, "contact", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/wkHasProprietorses", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the wkHasProprietorses instance associated with the given id.")
    public Page<WkHasProprietors> findAssociatedwkHasProprietorses(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated wkHasProprietorses");
        return wkHasProprietorsService.findAssociatedValues(id, "contact", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/cntAddresses", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the cntAddresses instance associated with the given id.")
    public Page<CntAddress> findAssociatedcntAddresses(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated cntAddresses");
        return cntAddressService.findAssociatedValues(id, "contact", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/wkSubmissionsForSubmittedPerson", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the wkSubmissionsForSubmittedPerson instance associated with the given id.")
    public Page<WkSubmission> findAssociatedwkSubmissionsForSubmittedPerson(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated wkSubmissionsForSubmittedPerson");
        return wkSubmissionService.findAssociatedValues(id, "contactBySubmittedCo", "contactid", pageable);
    }

    @RequestMapping(value = "/{id:.+}/cntTaxexemptions", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the cntTaxexemptions instance associated with the given id.")
    public Page<CntTaxexemption> findAssociatedcntTaxexemptions(Pageable pageable, @PathVariable("id") Integer id) {
        LOGGER.debug("Fetching all associated cntTaxexemptions");
        return cntTaxexemptionService.findAssociatedValues(id, "contact", "contactid", pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ApiOperation(value = "Creates a new Contact instance.")
    public Contact createContact(@RequestBody Contact instance) {
        LOGGER.debug("Create Contact with information: {}", instance);
        instance = contactService.create(instance);
        LOGGER.debug("Created Contact with information: {}", instance);
        return instance;
    }

    /**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
    protected void setContactService(ContactService service) {
        this.contactService = service;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of Contact instances.")
    public Long countAllContacts() {
        LOGGER.debug("counting Contacts");
        Long count = contactService.countAll();
        return count;
    }
}
