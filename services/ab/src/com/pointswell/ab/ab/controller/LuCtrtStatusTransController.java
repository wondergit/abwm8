/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import com.pointswell.ab.ab.service.LuCtrtStatusTransService;
import org.springframework.web.bind.annotation.RequestBody;
import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.TypeMismatchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.util.WMMultipartUtils;
import com.wavemaker.runtime.util.WMRuntimeUtils;
import com.wordnik.swagger.annotations.*;
import com.pointswell.ab.ab.*;
import com.pointswell.ab.ab.service.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class LuCtrtStatusTrans.
 * @see com.pointswell.ab.ab.LuCtrtStatusTrans
 */
@RestController(value = "Ab.LuCtrtStatusTransController")
@RequestMapping("/ab/LuCtrtStatusTrans")
@Api(description = "Exposes APIs to work with LuCtrtStatusTrans resource.", value = "LuCtrtStatusTransController")
public class LuCtrtStatusTransController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LuCtrtStatusTransController.class);

    @Autowired
    @Qualifier("ab.LuCtrtStatusTransService")
    private LuCtrtStatusTransService luCtrtStatusTransService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of LuCtrtStatusTrans instances matching the search criteria.")
    public Page<LuCtrtStatusTrans> findLuCtrtStatusTranss(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering LuCtrtStatusTranss list");
        return luCtrtStatusTransService.findAll(queryFilters, pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of LuCtrtStatusTrans instances.")
    public Page<LuCtrtStatusTrans> getLuCtrtStatusTranss(Pageable pageable) {
        LOGGER.debug("Rendering LuCtrtStatusTranss list");
        return luCtrtStatusTransService.findAll(pageable);
    }

    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the LuCtrtStatusTrans instance associated with the given composite-id.")
    public LuCtrtStatusTrans getLuCtrtStatusTrans(@RequestParam("luLanguageid") Integer luLanguageid, @RequestParam("luCtrtStatusid") Integer luCtrtStatusid) throws EntityNotFoundException {
        LuCtrtStatusTransId temp = new LuCtrtStatusTransId();
        temp.setLuLanguageid(luLanguageid);
        temp.setLuCtrtStatusid(luCtrtStatusid);
        LOGGER.debug("Getting LuCtrtStatusTrans with id: {}", temp);
        LuCtrtStatusTrans instance = luCtrtStatusTransService.findById(temp);
        LOGGER.debug("LuCtrtStatusTrans details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes the LuCtrtStatusTrans instance associated with the given composite-id.")
    public boolean deleteLuCtrtStatusTrans(@RequestParam("luLanguageid") Integer luLanguageid, @RequestParam("luCtrtStatusid") Integer luCtrtStatusid) throws EntityNotFoundException {
        LuCtrtStatusTransId temp = new LuCtrtStatusTransId();
        temp.setLuLanguageid(luLanguageid);
        temp.setLuCtrtStatusid(luCtrtStatusid);
        LOGGER.debug("Deleting LuCtrtStatusTrans with id: {}", temp);
        LuCtrtStatusTrans deleted = luCtrtStatusTransService.delete(temp);
        return deleted != null;
    }

    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates the LuCtrtStatusTrans instance associated with the given composite-id.")
    public LuCtrtStatusTrans editLuCtrtStatusTrans(@RequestParam("luLanguageid") Integer luLanguageid, @RequestParam("luCtrtStatusid") Integer luCtrtStatusid, @RequestBody LuCtrtStatusTrans instance) throws EntityNotFoundException {
        LuCtrtStatusTransId temp = new LuCtrtStatusTransId();
        temp.setLuLanguageid(luLanguageid);
        temp.setLuCtrtStatusid(luCtrtStatusid);
        luCtrtStatusTransService.delete(temp);
        instance = luCtrtStatusTransService.create(instance);
        LOGGER.debug("LuCtrtStatusTrans details with id is updated: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ApiOperation(value = "Creates a new LuCtrtStatusTrans instance.")
    public LuCtrtStatusTrans createLuCtrtStatusTrans(@RequestBody LuCtrtStatusTrans instance) {
        LOGGER.debug("Create LuCtrtStatusTrans with information: {}", instance);
        instance = luCtrtStatusTransService.create(instance);
        LOGGER.debug("Created LuCtrtStatusTrans with information: {}", instance);
        return instance;
    }

    /**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
    protected void setLuCtrtStatusTransService(LuCtrtStatusTransService service) {
        this.luCtrtStatusTransService = service;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of LuCtrtStatusTrans instances.")
    public Long countAllLuCtrtStatusTranss() {
        LOGGER.debug("counting LuCtrtStatusTranss");
        Long count = luCtrtStatusTransService.countAll();
        return count;
    }
}
