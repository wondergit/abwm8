/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import com.pointswell.ab.ab.service.WkCatalogueService;
import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.TypeMismatchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.util.WMMultipartUtils;
import com.wavemaker.runtime.util.WMRuntimeUtils;
import com.wordnik.swagger.annotations.*;
import com.pointswell.ab.ab.*;
import com.pointswell.ab.ab.service.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class WkCatalogue.
 * @see com.pointswell.ab.ab.WkCatalogue
 */
@RestController(value = "Ab.WkCatalogueController")
@RequestMapping("/ab/WkCatalogue")
@Api(description = "Exposes APIs to work with WkCatalogue resource.", value = "WkCatalogueController")
public class WkCatalogueController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WkCatalogueController.class);

    @Autowired
    @Qualifier("ab.WkCatalogueService")
    private WkCatalogueService wkCatalogueService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of WkCatalogue instances matching the search criteria.")
    public Page<WkCatalogue> findWkCatalogues(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering WkCatalogues list");
        return wkCatalogueService.findAll(queryFilters, pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of WkCatalogue instances.")
    public Page<WkCatalogue> getWkCatalogues(Pageable pageable) {
        LOGGER.debug("Rendering WkCatalogues list");
        return wkCatalogueService.findAll(pageable);
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the WkCatalogue instance associated with the given id.")
    public WkCatalogue getWkCatalogue(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting WkCatalogue with id: {}", id);
        WkCatalogue instance = wkCatalogueService.findById(id);
        LOGGER.debug("WkCatalogue details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes the WkCatalogue instance associated with the given id.")
    public boolean deleteWkCatalogue(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting WkCatalogue with id: {}", id);
        WkCatalogue deleted = wkCatalogueService.delete(id);
        return deleted != null;
    }

    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates the WkCatalogue instance associated with the given id.")
    public WkCatalogue editWkCatalogue(@PathVariable("id") Integer id, @RequestBody WkCatalogue instance) throws EntityNotFoundException {
        LOGGER.debug("Editing WkCatalogue with id: {}", instance.getWkCatalogueid());
        instance.setWkCatalogueid(id);
        instance = wkCatalogueService.update(instance);
        LOGGER.debug("WkCatalogue details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ApiOperation(value = "Creates a new WkCatalogue instance.")
    public WkCatalogue createWkCatalogue(@RequestBody WkCatalogue instance) {
        LOGGER.debug("Create WkCatalogue with information: {}", instance);
        instance = wkCatalogueService.create(instance);
        LOGGER.debug("Created WkCatalogue with information: {}", instance);
        return instance;
    }

    /**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
    protected void setWkCatalogueService(WkCatalogueService service) {
        this.wkCatalogueService = service;
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of WkCatalogue instances.")
    public Long countAllWkCatalogues() {
        LOGGER.debug("counting WkCatalogues");
        Long count = wkCatalogueService.countAll();
        return count;
    }
}
