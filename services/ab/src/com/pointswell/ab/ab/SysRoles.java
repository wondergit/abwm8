/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import javax.persistence.PrimaryKeyJoinColumn;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import java.util.Arrays;

import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.UniqueConstraint;




/**
 * SysRoles generated by hbm2java
 */
@Entity
@Table(name="sys_roles"
)
public class SysRoles  implements java.io.Serializable
 {


private Integer rolesid;
private String roleName;
private Set<SysGroupHasSysUsers> sysGroupHasSysUserses = new HashSet<SysGroupHasSysUsers>(0);

    public SysRoles() {
    }



     @Id 

    

    @Column(name="rolesid", nullable=false)
    public Integer getRolesid() {
        return this.rolesid;
    }
    
    public void setRolesid(Integer rolesid) {
        this.rolesid = rolesid;
    }

    

    @Column(name="role_name", nullable=false, length=45)
    public String getRoleName() {
        return this.roleName;
    }
    
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="sysRoles")
    public Set<SysGroupHasSysUsers> getSysGroupHasSysUserses() {
        return this.sysGroupHasSysUserses;
    }
    
    public void setSysGroupHasSysUserses(Set<SysGroupHasSysUsers> sysGroupHasSysUserses) {
        this.sysGroupHasSysUserses = sysGroupHasSysUserses;
    }



public boolean equals(Object o) {
         if (this == o)
         return true;
		 if ( (o == null ) )
		 return false;
		 if ( !(o instanceof SysRoles) )
		 return false;

		 SysRoles that = ( SysRoles ) o;

		 return ( (this.getRolesid()==that.getRolesid()) || ( this.getRolesid()!=null && that.getRolesid()!=null && this.getRolesid().equals(that.getRolesid()) ) );

   }

   public int hashCode() {
         int result = 17;

         result = 37 * result + ( getRolesid() == null ? 0 : this.getRolesid().hashCode() );

         return result;
     }


}

