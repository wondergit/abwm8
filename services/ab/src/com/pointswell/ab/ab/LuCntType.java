/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import javax.persistence.PrimaryKeyJoinColumn;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import java.util.Arrays;

import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.UniqueConstraint;




/**
 * LuCntType generated by hbm2java
 */
@Entity
@Table(name="lu_cnt_type"
)
public class LuCntType  implements java.io.Serializable
 {


private Integer luCntTypeid;
private String luCntType;
private Set<LuCntTypeTrans> luCntTypeTranses = new HashSet<LuCntTypeTrans>(0);
private Set<LuCntSubtypeTrans> luCntSubtypeTranses = new HashSet<LuCntSubtypeTrans>(0);
private Set<Contact> contacts = new HashSet<Contact>(0);

    public LuCntType() {
    }



     @Id @GeneratedValue(strategy=IDENTITY)

    

    @Column(name="lu_cnt_typeid", nullable=false)
    public Integer getLuCntTypeid() {
        return this.luCntTypeid;
    }
    
    public void setLuCntTypeid(Integer luCntTypeid) {
        this.luCntTypeid = luCntTypeid;
    }

    

    @Column(name="lu_cnt_type", nullable=false, length=15)
    public String getLuCntType() {
        return this.luCntType;
    }
    
    public void setLuCntType(String luCntType) {
        this.luCntType = luCntType;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luCntType")
    public Set<LuCntTypeTrans> getLuCntTypeTranses() {
        return this.luCntTypeTranses;
    }
    
    public void setLuCntTypeTranses(Set<LuCntTypeTrans> luCntTypeTranses) {
        this.luCntTypeTranses = luCntTypeTranses;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luCntType")
    public Set<LuCntSubtypeTrans> getLuCntSubtypeTranses() {
        return this.luCntSubtypeTranses;
    }
    
    public void setLuCntSubtypeTranses(Set<LuCntSubtypeTrans> luCntSubtypeTranses) {
        this.luCntSubtypeTranses = luCntSubtypeTranses;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luCntType")
    public Set<Contact> getContacts() {
        return this.contacts;
    }
    
    public void setContacts(Set<Contact> contacts) {
        this.contacts = contacts;
    }



public boolean equals(Object o) {
         if (this == o)
         return true;
		 if ( (o == null ) )
		 return false;
		 if ( !(o instanceof LuCntType) )
		 return false;

		 LuCntType that = ( LuCntType ) o;

		 return ( (this.getLuCntTypeid()==that.getLuCntTypeid()) || ( this.getLuCntTypeid()!=null && that.getLuCntTypeid()!=null && this.getLuCntTypeid().equals(that.getLuCntTypeid()) ) );

   }

   public int hashCode() {
         int result = 17;

         result = 37 * result + ( getLuCntTypeid() == null ? 0 : this.getLuCntTypeid().hashCode() );

         return result;
     }


}

