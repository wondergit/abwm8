/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import javax.persistence.PrimaryKeyJoinColumn;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import java.util.Arrays;

import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.UniqueConstraint;




/**
 * WkPermission generated by hbm2java
 */
@Entity
@Table(name="wk_permission"
)
public class WkPermission  implements java.io.Serializable
 {


private WkPermissionId id;
private String permission;
private Date dateobtained;
private Date xcreateddate;
private String xcreatedby;
private Date xmodifieddate;
private String xmodifiedby;
private Integer qty;
private String linktolicence;
private Integer sysProcessLogid;
private LuDelTypes luDelTypes;
private LuRightsType luRightsType;
private Tenant tenant;
private Work work;
private Set<WkPermHasCty> wkPermHasCties = new HashSet<WkPermHasCty>(0);
private Set<WkPermHasLanguage> wkPermHasLanguages = new HashSet<WkPermHasLanguage>(0);

    public WkPermission() {
    }



     @EmbeddedId

    

    @AttributeOverrides( {
        @AttributeOverride(name="workPermissionid", column=@Column(name="work_permissionid", nullable=false) ), 
        @AttributeOverride(name="workid", column=@Column(name="workid", nullable=false) ) } )
    public WkPermissionId getId() {
        return this.id;
    }
    
    public void setId(WkPermissionId id) {
        this.id = id;
    }

    

    @Column(name="permission", nullable=false, length=65535)
    public String getPermission() {
        return this.permission;
    }
    
    public void setPermission(String permission) {
        this.permission = permission;
    }

    @Temporal(TemporalType.DATE)

    @Column(name="dateobtained", length=10)
    public Date getDateobtained() {
        return this.dateobtained;
    }
    
    public void setDateobtained(Date dateobtained) {
        this.dateobtained = dateobtained;
    }

    @Temporal(TemporalType.TIMESTAMP)

    @Column(name="xcreateddate", nullable=false, length=19)
    public Date getXcreateddate() {
        return this.xcreateddate;
    }
    
    public void setXcreateddate(Date xcreateddate) {
        this.xcreateddate = xcreateddate;
    }

    

    @Column(name="xcreatedby", nullable=false, length=45)
    public String getXcreatedby() {
        return this.xcreatedby;
    }
    
    public void setXcreatedby(String xcreatedby) {
        this.xcreatedby = xcreatedby;
    }

    @Temporal(TemporalType.TIMESTAMP)

    @Column(name="xmodifieddate", length=19)
    public Date getXmodifieddate() {
        return this.xmodifieddate;
    }
    
    public void setXmodifieddate(Date xmodifieddate) {
        this.xmodifieddate = xmodifieddate;
    }

    

    @Column(name="xmodifiedby", length=45)
    public String getXmodifiedby() {
        return this.xmodifiedby;
    }
    
    public void setXmodifiedby(String xmodifiedby) {
        this.xmodifiedby = xmodifiedby;
    }

    

    @Column(name="qty")
    public Integer getQty() {
        return this.qty;
    }
    
    public void setQty(Integer qty) {
        this.qty = qty;
    }

    

    @Column(name="linktolicence", length=200)
    public String getLinktolicence() {
        return this.linktolicence;
    }
    
    public void setLinktolicence(String linktolicence) {
        this.linktolicence = linktolicence;
    }

    

    @Column(name="sys_process_logid")
    public Integer getSysProcessLogid() {
        return this.sysProcessLogid;
    }
    
    public void setSysProcessLogid(Integer sysProcessLogid) {
        this.sysProcessLogid = sysProcessLogid;
    }

@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="lookup_del_typesid", nullable=false)
    public LuDelTypes getLuDelTypes() {
        return this.luDelTypes;
    }
    
    public void setLuDelTypes(LuDelTypes luDelTypes) {
        this.luDelTypes = luDelTypes;
    }

@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="lookup_rightstypeid", nullable=false)
    public LuRightsType getLuRightsType() {
        return this.luRightsType;
    }
    
    public void setLuRightsType(LuRightsType luRightsType) {
        this.luRightsType = luRightsType;
    }

@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="tenantid", nullable=false)
    public Tenant getTenant() {
        return this.tenant;
    }
    
    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="workid", nullable=false, insertable=false, updatable=false)
    public Work getWork() {
        return this.work;
    }
    
    public void setWork(Work work) {
        this.work = work;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="wkPermission")
    public Set<WkPermHasCty> getWkPermHasCties() {
        return this.wkPermHasCties;
    }
    
    public void setWkPermHasCties(Set<WkPermHasCty> wkPermHasCties) {
        this.wkPermHasCties = wkPermHasCties;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="wkPermission")
    public Set<WkPermHasLanguage> getWkPermHasLanguages() {
        return this.wkPermHasLanguages;
    }
    
    public void setWkPermHasLanguages(Set<WkPermHasLanguage> wkPermHasLanguages) {
        this.wkPermHasLanguages = wkPermHasLanguages;
    }



public boolean equals(Object o) {
         if (this == o)
         return true;
		 if ( (o == null ) )
		 return false;
		 if ( !(o instanceof WkPermission) )
		 return false;

		 WkPermission that = ( WkPermission ) o;

		 return ( (this.getId()==that.getId()) || ( this.getId()!=null && that.getId()!=null && this.getId().equals(that.getId()) ) );

   }

   public int hashCode() {
         int result = 17;

         result = 37 * result + ( getId() == null ? 0 : this.getId().hashCode() );

         return result;
     }


}

