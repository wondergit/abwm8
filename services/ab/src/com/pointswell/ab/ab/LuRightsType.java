/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import javax.persistence.PrimaryKeyJoinColumn;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import java.util.Arrays;

import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.UniqueConstraint;




/**
 * LuRightsType generated by hbm2java
 */
@Entity
@Table(name="lu_rights_type"
)
public class LuRightsType  implements java.io.Serializable
 {


private Integer luRightsTypeid;
private String luRightsTypeName;
private Boolean luRightsTypeIsSubright;
private Set<WkPermission> wkPermissions = new HashSet<WkPermission>(0);
private Set<LuRightsTypeTrans> luRightsTypeTranses = new HashSet<LuRightsTypeTrans>(0);
private Set<CtrtSubRightsGranted> ctrtSubRightsGranteds = new HashSet<CtrtSubRightsGranted>(0);
private Set<CntDefaultcomm> cntDefaultcomms = new HashSet<CntDefaultcomm>(0);
private Set<CtrtFormat> ctrtFormats = new HashSet<CtrtFormat>(0);

    public LuRightsType() {
    }



     @Id @GeneratedValue(strategy=IDENTITY)

    

    @Column(name="lu_rights_typeid", nullable=false)
    public Integer getLuRightsTypeid() {
        return this.luRightsTypeid;
    }
    
    public void setLuRightsTypeid(Integer luRightsTypeid) {
        this.luRightsTypeid = luRightsTypeid;
    }

    

    @Column(name="lu_rights_type_name", nullable=false, length=45)
    public String getLuRightsTypeName() {
        return this.luRightsTypeName;
    }
    
    public void setLuRightsTypeName(String luRightsTypeName) {
        this.luRightsTypeName = luRightsTypeName;
    }

    

    @Column(name="lu_rights_type_is_subright")
    public Boolean getLuRightsTypeIsSubright() {
        return this.luRightsTypeIsSubright;
    }
    
    public void setLuRightsTypeIsSubright(Boolean luRightsTypeIsSubright) {
        this.luRightsTypeIsSubright = luRightsTypeIsSubright;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luRightsType")
    public Set<WkPermission> getWkPermissions() {
        return this.wkPermissions;
    }
    
    public void setWkPermissions(Set<WkPermission> wkPermissions) {
        this.wkPermissions = wkPermissions;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luRightsType")
    public Set<LuRightsTypeTrans> getLuRightsTypeTranses() {
        return this.luRightsTypeTranses;
    }
    
    public void setLuRightsTypeTranses(Set<LuRightsTypeTrans> luRightsTypeTranses) {
        this.luRightsTypeTranses = luRightsTypeTranses;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luRightsType")
    public Set<CtrtSubRightsGranted> getCtrtSubRightsGranteds() {
        return this.ctrtSubRightsGranteds;
    }
    
    public void setCtrtSubRightsGranteds(Set<CtrtSubRightsGranted> ctrtSubRightsGranteds) {
        this.ctrtSubRightsGranteds = ctrtSubRightsGranteds;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luRightsType")
    public Set<CntDefaultcomm> getCntDefaultcomms() {
        return this.cntDefaultcomms;
    }
    
    public void setCntDefaultcomms(Set<CntDefaultcomm> cntDefaultcomms) {
        this.cntDefaultcomms = cntDefaultcomms;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luRightsType")
    public Set<CtrtFormat> getCtrtFormats() {
        return this.ctrtFormats;
    }
    
    public void setCtrtFormats(Set<CtrtFormat> ctrtFormats) {
        this.ctrtFormats = ctrtFormats;
    }



public boolean equals(Object o) {
         if (this == o)
         return true;
		 if ( (o == null ) )
		 return false;
		 if ( !(o instanceof LuRightsType) )
		 return false;

		 LuRightsType that = ( LuRightsType ) o;

		 return ( (this.getLuRightsTypeid()==that.getLuRightsTypeid()) || ( this.getLuRightsTypeid()!=null && that.getLuRightsTypeid()!=null && this.getLuRightsTypeid().equals(that.getLuRightsTypeid()) ) );

   }

   public int hashCode() {
         int result = 17;

         result = 37 * result + ( getLuRightsTypeid() == null ? 0 : this.getLuRightsTypeid().hashCode() );

         return result;
     }


}

