/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import javax.persistence.PrimaryKeyJoinColumn;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import java.util.Arrays;

import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.UniqueConstraint;




/**
 * LuDerivationType generated by hbm2java
 */
@Entity
@Table(name="lu_derivation_type"
)
public class LuDerivationType  implements java.io.Serializable
 {


private Integer luDerivationtypeid;
private String luDerivationtypeistccode;
private String luDerivationtypeName;
private Set<LuDerivationTypeTrans> luDerivationTypeTranses = new HashSet<LuDerivationTypeTrans>(0);
private Set<Work> works = new HashSet<Work>(0);

    public LuDerivationType() {
    }



     @Id @GeneratedValue(strategy=IDENTITY)

    

    @Column(name="lu_derivationtypeid", nullable=false)
    public Integer getLuDerivationtypeid() {
        return this.luDerivationtypeid;
    }
    
    public void setLuDerivationtypeid(Integer luDerivationtypeid) {
        this.luDerivationtypeid = luDerivationtypeid;
    }

    

    @Column(name="lu_derivationtypeistccode", nullable=false, length=2)
    public String getLuDerivationtypeistccode() {
        return this.luDerivationtypeistccode;
    }
    
    public void setLuDerivationtypeistccode(String luDerivationtypeistccode) {
        this.luDerivationtypeistccode = luDerivationtypeistccode;
    }

    

    @Column(name="lu_derivationtype_name", nullable=false, length=45)
    public String getLuDerivationtypeName() {
        return this.luDerivationtypeName;
    }
    
    public void setLuDerivationtypeName(String luDerivationtypeName) {
        this.luDerivationtypeName = luDerivationtypeName;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luDerivationType")
    public Set<LuDerivationTypeTrans> getLuDerivationTypeTranses() {
        return this.luDerivationTypeTranses;
    }
    
    public void setLuDerivationTypeTranses(Set<LuDerivationTypeTrans> luDerivationTypeTranses) {
        this.luDerivationTypeTranses = luDerivationTypeTranses;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="luDerivationType")
    public Set<Work> getWorks() {
        return this.works;
    }
    
    public void setWorks(Set<Work> works) {
        this.works = works;
    }



public boolean equals(Object o) {
         if (this == o)
         return true;
		 if ( (o == null ) )
		 return false;
		 if ( !(o instanceof LuDerivationType) )
		 return false;

		 LuDerivationType that = ( LuDerivationType ) o;

		 return ( (this.getLuDerivationtypeid()==that.getLuDerivationtypeid()) || ( this.getLuDerivationtypeid()!=null && that.getLuDerivationtypeid()!=null && this.getLuDerivationtypeid().equals(that.getLuDerivationtypeid()) ) );

   }

   public int hashCode() {
         int result = 17;

         result = 37 * result + ( getLuDerivationtypeid() == null ? 0 : this.getLuDerivationtypeid().hashCode() );

         return result;
     }


}

