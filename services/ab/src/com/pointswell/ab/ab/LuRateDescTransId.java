/*Copyright (c) 2015-2016 pointswell-com All Rights Reserved.

This software is the confidential and proprietary information of pointswell-com You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with pointswell-com*/

package com.pointswell.ab.ab;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Column;
import javax.persistence.Embeddable;


import java.util.Arrays;

import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.UniqueConstraint;




/**
 * LuRateDescTransId generated by hbm2java
 */
@Embeddable
public class LuRateDescTransId  implements java.io.Serializable
 {


private Integer luLanguageid;
private Integer luRateDescriptionid;

    public LuRateDescTransId() {
    }




    

    @Column(name="lu_languageid", nullable=false)
    public Integer getLuLanguageid() {
        return this.luLanguageid;
    }
    
    public void setLuLanguageid(Integer luLanguageid) {
        this.luLanguageid = luLanguageid;
    }

    

    @Column(name="lu_rate_descriptionid", nullable=false)
    public Integer getLuRateDescriptionid() {
        return this.luRateDescriptionid;
    }
    
    public void setLuRateDescriptionid(Integer luRateDescriptionid) {
        this.luRateDescriptionid = luRateDescriptionid;
    }



public boolean equals(Object o) {
         if (this == o)
         return true;
		 if ( (o == null ) )
		 return false;
		 if ( !(o instanceof LuRateDescTransId) )
		 return false;

		 LuRateDescTransId that = ( LuRateDescTransId ) o;

         return ( (this.getLuLanguageid()==that.getLuLanguageid()) || ( this.getLuLanguageid()!=null && that.getLuLanguageid()!=null && this.getLuLanguageid().equals(that.getLuLanguageid()) ) )
 && ( (this.getLuRateDescriptionid()==that.getLuRateDescriptionid()) || ( this.getLuRateDescriptionid()!=null && that.getLuRateDescriptionid()!=null && this.getLuRateDescriptionid().equals(that.getLuRateDescriptionid()) ) );

   }

   public int hashCode() {
         int result = 17;

         result = 37 * result + ( getLuLanguageid() == null ? 0 : this.getLuLanguageid().hashCode() );
         result = 37 * result + ( getLuRateDescriptionid() == null ? 0 : this.getLuRateDescriptionid().hashCode() );

         return result;
     }


}

